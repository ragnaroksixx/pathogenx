﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorUnlock : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            //Actives Menu Canvas
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }
}
