﻿using UnityEngine;
using System.Collections;

public class StatsScreenButton : MonoBehaviour
{
    public void OpenStatsMenu()
    {
        Publisher.Raise(new ShowStatsEvent());
    }
}
