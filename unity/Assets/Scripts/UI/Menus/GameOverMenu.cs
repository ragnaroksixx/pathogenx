﻿using UnityEngine;
using System.Collections;

public class GameOverMenu : Menu
{
    protected override void Awake()
    {
        base.Awake();
        Publisher.Subscribe<GameOverEvent>(OnGameOver);
    }
    private void OnDestroy()
    {
        Publisher.Unsubscribe<GameOverEvent>(OnGameOver);
    }
    void OnGameOver(GameOverEvent e)
    {
        Show();
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    public void MainMenu()
    {
        SceneController.LoadInitScene();
        Hide(true);
    }
    public void TryAgain()
    {
        LevelManager.ReloadScene();
        Hide(true);
    }
}

public class GameOverEvent : PublisherEvent
{

}
