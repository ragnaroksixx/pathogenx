﻿using UnityEngine;
using System.Collections;

public class HealthSpawnHelper : SpawnHelper
{
    public override float GetSpawnMultiplier(SpawnData sd)
    {
        float val = base.GetSpawnMultiplier(sd);
        SimpleItemPickUp pickup = sd.obj.GetComponentInChildren<SimpleItemPickUp>();
        if (pickup && pickup.item.Contains("medkit"))
        {
            if (Inventory.Instance.GetItem("medkit").GetCount() > 2)
                val = 0.3f;
            if (Inventory.Instance.GetItem("medkit").GetCount() > 0)
                val = 0.7f;
            else if (Player.Instance.health.Normalized() < .25f)
                return 1.5f;
        }
        else if (sd.obj.name.Contains("empty"))
        {
            if (Player.Instance.health.Normalized() < .25f)
                return 0.8f;
            else if (Player.Instance.health.Normalized() < .5f)
                return 0.95f;
        }
        return val;
    }
}
