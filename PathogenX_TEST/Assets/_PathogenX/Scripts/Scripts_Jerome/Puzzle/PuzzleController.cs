﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleController : MonoBehaviour
{
    public enum PuzzleType
    {
        RANDOM = -1,
        NONE = 0,
        KEY,
        KEYPAD
    }
    public PuzzleType pt;
    public Puzzle[] puzzles;

    public Puzzle GetPuzzle()
    {
        if (pt == PuzzleType.RANDOM)
            return puzzles[Random.Range(0, puzzles.Length)];
        return puzzles[(int)pt];
    }
}
