﻿using UnityEngine;
using System.Collections;

public abstract class Interactable : MonoBehaviour
{
    public CustomAudioClip onEnter;
    public string interactName;
    protected bool isActive = true;
    bool didEnter = false;
    protected GameObject player = null;
    public abstract void OnEnter(Collider other);
    public abstract void OnExit(Collider other);
    private void OnTriggerEnter(Collider other)
    {
        if (isActive && other.gameObject.tag == "Player")
        {
            player = other.gameObject;
            OnEnter(other);
            didEnter = true;
            Publisher.Raise(new InteractEvent(this, InteractType.ENTER));
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag != "Player")
            return;
        if (didEnter)
        {
            OnExit(other);
            Publisher.Raise(new HideNotificationEvent());
        }
        didEnter = false;
    }
    public virtual string GetText()
    {
        return interactName;
    }
}
