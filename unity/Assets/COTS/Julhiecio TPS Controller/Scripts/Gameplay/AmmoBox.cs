﻿using UnityEngine;

[AddComponentMenu("Julhiecio TPS Controller/Gameplay/Ammo Box")]
public class AmmoBox : SimpleKeyInteractable
{
    public string item;
    public TypeOfAmmo AmmoType;
    public int AmmoCount = 32;
    public enum TypeOfAmmo { AnyGun, Pistol, Rifle, Shotgun }
    public RPGTalkArea RPGTalkArea;
    public override void OnPress()
    {
        if (Inventory.Instance.IsFull() && !Inventory.Instance.HasItem(item))
        {
            Publisher.Raise(new NotificationEvent("<color=red>Inventory Full"));
            return;
        }
        RPGTalkArea?.StartTalk();
        WinMenu.AddStat("items", 1);
        WinMenu.AddStat("score", 50);
        PlayerPickUpHack(Inventory.Instance.player);
        Publisher.Raise(new ItemPickUpEvent(item));
        Publisher.Raise(new HideNotificationEvent());
    }
    void PlayerPickUpHack(ThirdPersonController tpc)
    {
        if (AmmoType == TypeOfAmmo.AnyGun && tpc.IsArmed)
        {
            tpc.WeaponInUse.TotalBullets += AmmoCount;
            Destroy(this.gameObject);
        }

        if (AmmoType == TypeOfAmmo.Pistol)
        {
            for (int i = 0; i < tpc.Weapons.Length; i++)
            {
                if (tpc.Weapons[i].Type == Weapon.WeaponType.Pistol)
                {
                    tpc.Weapons[i].TotalBullets += AmmoCount;
                }
            }
            Destroy(this.gameObject);
        }

        if (AmmoType == TypeOfAmmo.Rifle)
        {
            for (int i = 0; i < tpc.Weapons.Length; i++)
            {
                if (tpc.Weapons[i].Type == Weapon.WeaponType.Rifle)
                {
                    tpc.Weapons[i].TotalBullets += AmmoCount;
                }
            }
            Destroy(this.gameObject);
        }

        if (AmmoType == TypeOfAmmo.Shotgun)
        {
            for (int i = 0; i < tpc.Weapons.Length; i++)
            {
                if (tpc.Weapons[i].Type == Weapon.WeaponType.Shotgun)
                {
                    tpc.Weapons[i].TotalBullets += AmmoCount;
                }
            }
            Destroy(this.gameObject);

        }
    }
}
