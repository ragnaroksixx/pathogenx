﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public bool didSpawn = false;
    public SpawnData[] spawnables;
    public bool spawnOnAwake;
    public bool multiSpawn;
    public bool overlapHack;
    public bool detachFromSpawner;
    public bool ignoreRefresh = false;
    GameObject spawned;
    SpawnHelper[] helpers;
    private void Awake()
    {
        if (!ignoreRefresh)
            Publisher.Subscribe<RefreshSpawnsEvent>(RefreshSpawn);
        helpers = GetComponents<SpawnHelper>();
        if (spawnOnAwake)
            Spawn();
    }
    private void OnDestroy()
    {
        Publisher.Unsubscribe<RefreshSpawnsEvent>(RefreshSpawn);
    }
    public virtual void Spawn()
    {
        if (didSpawn && !multiSpawn) return;
        if (overlapHack && spawned != null)
        {
            return;
        }
        didSpawn = true;
        int sum = 0;
        foreach (SpawnData sData in spawnables)
        {
            sum += GetSpawnRate(sData);
        }

        float randomWeight = 0;
        //No weight on any number?
        if (sum == 0)
            return;
        randomWeight = UnityEngine.Random.Range(0, sum);

        GameObject val = null;
        foreach (SpawnData sData in spawnables)
        {
            int sRate = GetSpawnRate(sData);
            if (randomWeight < sRate)
            {
                val = sData.obj;
                break;
            }
            randomWeight -= sRate;
        }

        if (val)
        {
            spawned = val;
            val = GameObject.Instantiate(val, transform.position, transform.rotation);
            if (!detachFromSpawner)
                val.transform.parent = transform;
            ISpawnable ispawn = val.GetComponent<ISpawnable>();
            if (ispawn != null)
            {
                ispawn.OnSpawn(this);
            }
        }
    }
    public void RefreshSpawn(RefreshSpawnsEvent e)
    {
        if (!ignoreRefresh)
            didSpawn = false;
    }
    public int GetSpawnRate(SpawnData sd)
    {
        float mult = 1;
        foreach (SpawnHelper sh in helpers)
        {
            mult *= sh.GetSpawnMultiplier(sd);
        }
        return Mathf.FloorToInt(sd.spawnRate * mult);
    }
}
[System.Serializable]
public struct SpawnData
{
    public GameObject obj;
    [Range(0, 100)]
    [Tooltip("100 is required spawn")]
    public int spawnRate;
}
public interface ISpawnable
{
    void OnSpawn(Spawner s);
}
public class RefreshSpawnsEvent : PublisherEvent
{

}

