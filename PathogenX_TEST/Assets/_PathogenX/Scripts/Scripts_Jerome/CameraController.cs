﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
    private void OnEnable()
    {
        Publisher.Subscribe<FixedCameraEvent>(SetPose);
    }
    private void OnDisable()
    {
        Publisher.Unsubscribe<FixedCameraEvent>(SetPose);
    }

    public void SetPose(FixedCameraEvent e)
    {
        transform.position = e.location.position;
        transform.rotation = e.location.rotation;
    }
}
