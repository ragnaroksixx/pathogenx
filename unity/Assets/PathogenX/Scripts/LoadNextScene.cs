﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadNextScene : MonoBehaviour
{
    public string nextScene;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    public void LoadScene()
    {
        SceneController.LoadScenesAdditive(nextScene);
        SceneManager.UnloadSceneAsync(gameObject.scene.name, UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);
    }
}
