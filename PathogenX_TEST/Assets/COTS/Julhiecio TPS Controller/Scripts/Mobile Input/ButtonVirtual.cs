﻿using UnityEngine;
using UnityEngine.EventSystems;

[AddComponentMenu("Julhiecio TPS Controller /Mobile/Button")]
public class ButtonVirtual : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public bool IsPressed;
    public void OnPointerDown(PointerEventData e)
    {
        IsPressed = true;
    }

    public void OnPointerUp(PointerEventData e)
    {
        IsPressed = false;
    }
}
