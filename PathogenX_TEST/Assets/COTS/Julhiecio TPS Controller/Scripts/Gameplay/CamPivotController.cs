﻿using System.Collections;
using UnityEngine;
[AddComponentMenu("Julhiecio TPS Controller /Gameplay/Camera Controller")]
public class CamPivotController : MonoBehaviour
{
    [Header("Camera Target")]
    public ThirdPersonController PlayerTarget;
    //[HideInInspector] public Camera mCamera;
    public Camera mCamera;

    [Header("Settings")]
    public bool IsMobile;
    private Touchfield touchfield;
    private Touchfield touch_shootbutton;

    public float VelocityCamera = 10;
    public bool CameraShakeWhenShooting = true;

    [Range(0, 2)] public float CameraShakeSensibility = 0.5f;
    [HideInInspector] private bool SmothCamera;

    public LayerMask CameraColissionLayer;
    public LayerMask CameraCollisionDrivingLayer;

    public float Sensibility = 150f;
    public float Distance = 3f;
    public float IsArmedDistance = 1f;

    [HideInInspector]
    float ActualDistance = 5;

    [Header("Limit FPS")]
    public int FPS_Limit;

    [Header("Camera Position Adjustment")]
    public bool CrouchCamera;
    public float TargetHeight = 1.3f;
    public float TargetHeightCrouched = 0.9f;
    float StartTargetHeight;
    public float X_adjust = 0.5f;
    public float Y_adjust = 0.4f;

    [Header("Camera Position Driving Adjustment")]
    public float DistanceCarDriving = 6f;
    public float DistanceMotocycleDriving = 3f;

    public float TargetHeightDriving = 1.3f;
    public float X_adjustDriving = 0f;
    public float Y_adjustDriving = 0.4f;
    public float Z_adjustDriving = 0.4f;

    [Header("Camera Rotation Limit")]
    public float MinRotation = -80f;
    public float MaxRotation = 80f;

    //Camera Rotation Axis
    [HideInInspector]
    public float rotX;
    [HideInInspector]
    public float rotY;
    float xmouse;
    float ymouse;
    [HideInInspector]
    public float rotxtarget;

    Vector3 PositionTarget;

    IEnumerator Start()
    {
        UpdateSensitivity();
        StartTargetHeight = TargetHeight;
        while (PlayerTarget == null)
        {
            PlayerTarget = GameObject.FindGameObjectWithTag("Player").GetComponent<ThirdPersonController>();
            yield return null;
        }
        mCamera = Camera.main;

        if (IsMobile)
        {
            //Find Touch
            touchfield = GameObject.Find("Touchfield").GetComponent<Touchfield>();
            touch_shootbutton = GameObject.Find("ShotButton").GetComponent<Touchfield>();
        }
        else
        {
            ///Lock Mouse
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        Application.targetFrameRate = FPS_Limit;
    }
    void LateUpdate()
    {
        if (PlayerTarget.IsDead == true)
        {
            transform.position = PlayerTarget.HumanoidSpine.position;

            mCamera.transform.position = PlayerTarget.HumanoidSpine.position - transform.forward * Distance + transform.right;

            mCamera.transform.LookAt(PlayerTarget.HumanoidSpine.position);
        }

        if (PlayerTarget.IsDead == false)
            //CAMERA FALLOW
            if (SmothCamera == false && PlayerTarget.IsDriving == true)
            {
                transform.position = PlayerTarget.VehicleInArea.transform.position - PlayerTarget.transform.forward * Z_adjustDriving + PlayerTarget.transform.up * TargetHeightDriving;
            }

        //CAMERA HEIGHT ADJUSTMENT
        if (CrouchCamera == true)
        {
            if (PlayerTarget.IsCrouched == true)
            {
                TargetHeight = Mathf.Lerp(TargetHeight, TargetHeightCrouched, 5 * Time.deltaTime);
            }
            else
            {
                TargetHeight = Mathf.Lerp(TargetHeight, StartTargetHeight, 5 * Time.deltaTime);
            }
        }
        //SMOTH CAMERA DISTANCE
        if (PlayerTarget.IsArmed)
        {
            ActualDistance = Mathf.Lerp(ActualDistance, IsArmedDistance, 5 * Time.deltaTime);
        }
        else
        {
            ActualDistance = Mathf.Lerp(ActualDistance, Distance, 5 * Time.deltaTime);
        }
        //DRIVING POSITION ADJUSTMENT
        if (PlayerTarget.IsDriving == false)
        {
            mCamera.transform.position = transform.position - transform.forward * ActualDistance + transform.right * X_adjust + transform.up * Y_adjust;
        }
        if (PlayerTarget.IsDriving == true)
        {
            //DRIVING DISTANCE ADJUSTMENT
            if (PlayerTarget.VehicleInArea.TypeOfVehicle == Vehicle.VehicleType.Car)
            {
                mCamera.transform.position = transform.position - transform.forward * DistanceCarDriving + transform.right * X_adjustDriving + transform.up * Y_adjustDriving;
            }
            else
            {
                mCamera.transform.position = transform.position - transform.forward * DistanceMotocycleDriving + transform.right * X_adjustDriving + transform.up * Y_adjustDriving;
            }
        }

        //CAMERA COLLISION
        RaycastHit hit;
        if (PlayerTarget.IsDriving == false)
        {
            if (Physics.Linecast(transform.position, mCamera.transform.position, out hit, CameraColissionLayer))
            {
                mCamera.transform.position = hit.point + transform.forward * 0.2f;
            }
        }
        else
        {
            if (Physics.Linecast(transform.position, mCamera.transform.position, out hit, CameraCollisionDrivingLayer))
            {
                mCamera.transform.position = hit.point + transform.forward * 0.2f;
            }
        }


    }
    public void Update()
    {
        if (TimeHandler.Instance.isPaused)
            return;
        if (DoorTransport.hackLockCameraInDoor)
            return;
        if (PlayerTarget.IsDead == false)
        {
            //Camera Rotation
            if (IsMobile == false)
            {
                xmouse = Input.GetAxis("Mouse Y");
                ymouse = Input.GetAxis("Mouse X");
            }
            else
            {
                xmouse = touchfield.TouchDistance.y / 10 + touch_shootbutton.TouchDistance.y / 10;
                ymouse = touchfield.TouchDistance.x / 10 + touch_shootbutton.TouchDistance.x / 10;
            }
            rotxtarget -= xmouse * Sensibility;
            rotxtarget = Mathf.Clamp(rotxtarget, MinRotation, MaxRotation);

            rotX = Mathf.Lerp(rotX, rotxtarget, 30 * Time.fixedDeltaTime);
            rotY += ymouse * Sensibility;


            var rot = new Vector3(rotX, rotY, 0);
            transform.eulerAngles = rot;

            //Camera Change Movement Mode
            SmothCamera = !PlayerTarget.IsDriving;
        }
    }
    void FixedUpdate()
    {
        if (TimeHandler.Instance.isPaused)
            return;
        if (PlayerTarget.IsDead == false)
            if (SmothCamera == true)
            {
                transform.position = Vector3.Lerp(transform.position, PlayerTarget.transform.position + PlayerTarget.transform.up * TargetHeight, VelocityCamera * Time.deltaTime);
            }
    }
    void OnDrawGizmos()
    {
        var poscam = mCamera.transform.position;
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, poscam);
        Gizmos.DrawSphere(poscam, 0.1f);
    }

    public void UpdateSensitivity()
    {
        if (PlayerPrefs.HasKey("sensitivity"))
            Sensibility = PlayerPrefs.GetFloat("sensitivity", 1);
    }
}