﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "Area Data", menuName = "Level/Puzzle")]
public class Puzzle : ScriptableObject
{
    public SideRouteData sideRoute;
    public GameObject doorPrefab;
}

