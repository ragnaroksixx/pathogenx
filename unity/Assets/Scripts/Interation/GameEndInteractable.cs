﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameEndInteractable : SimpleKeyInteractable
{
    public override void OnPress()
    {
        Publisher.Raise(new NotificationEvent(""));
        LevelManager.EndLevel();
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        onEnter?.Play();
        SceneController.LoadScenesAdditive("Ralf_Cutscene_Typing_END");
        SceneManager.UnloadSceneAsync(gameObject.scene.name, UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);
    }
}

