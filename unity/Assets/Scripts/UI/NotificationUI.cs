﻿using UnityEngine;
using System.Collections;
using TMPro;
using DG.Tweening;

public class NotificationUI : MonoBehaviour
{
    public TMP_Text text;
    public CanvasGroup cGroup;
    float initFade;
    private void OnEnable()
    {
        Publisher.Subscribe<InteractEvent>(UpdateUI);
        Publisher.Subscribe<NotificationEvent>(UpdateUI);
        Publisher.Subscribe<HideNotificationEvent>(UpdateUI);
        initFade = cGroup.alpha;
        cGroup.alpha = 0;
    }
    private void OnDisable()
    {
        Publisher.Unsubscribe<InteractEvent>(UpdateUI);
        Publisher.Unsubscribe<NotificationEvent>(UpdateUI);
        Publisher.Unsubscribe<HideNotificationEvent>(UpdateUI);
    }

    public void UpdateUI(InteractEvent e)
    {
        if (string.IsNullOrEmpty(e.interact.interactName))
            return;
        text.text = e.interact.GetText();
        FadeIn();
    }
    public void UpdateUI(NotificationEvent e)
    {
        text.text = e.text;
        FadeIn();
    }
    public void UpdateUI(HideNotificationEvent e)
    {
        cGroup.DOKill();
        cGroup.DOFade(0, 0.2f);
    }
    public void FadeIn()
    {
        cGroup.DOKill();
        cGroup.DOFade(initFade, 0.2f);
    }
}

public class InteractEvent : PublisherEvent
{
    public Interactable interact;
    public InteractType iType;

    public InteractEvent(Interactable i, InteractType iType)
    {
        this.interact = i;
        this.iType = iType;
    }
}
public class NotificationEvent : PublisherEvent
{
    public string text;

    public NotificationEvent(string text)
    {
        this.text = text;
    }
}
public class HideNotificationEvent : PublisherEvent
{
    public HideNotificationEvent()
    {
    }
}
public enum InteractType
{
    ENTER,
    EXIT,
    USE
}
