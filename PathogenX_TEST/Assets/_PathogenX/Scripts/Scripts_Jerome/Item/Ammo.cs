﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Ammo : Item
{
    public Weapon.WeaponType weaponType;
    public override int GetCount()
    {
        if (Inventory.Instance == null)
            return 0;
        qty = 0;
        for (int i = 0; i < Inventory.Instance.player.Weapons.Length; i++)
        {
            if (Inventory.Instance.player.Weapons[i].Type == weaponType)
            {
                qty += Inventory.Instance.player.Weapons[i].TotalBullets;
            }
        }
        return base.GetCount();
    }
    public override void Discard()
    {
        for (int i = 0; i < Inventory.Instance.player.Weapons.Length; i++)
        {
            if (Inventory.Instance.player.Weapons[i].Type == weaponType)
            {
                Inventory.Instance.player.Weapons[i].TotalBullets = 0;
            }
        }
        base.Discard();
    }
}
