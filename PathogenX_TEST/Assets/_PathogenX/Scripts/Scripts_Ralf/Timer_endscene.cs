﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class Timer_endscene : MonoBehaviour
{
    public string LevelToLoad;
    public float timer = 44f;
    public bool keepCurrentScene;
    public UnityEvent onComplete;
    // Use this for initialization
    //void Start()
    //{
    //    timerSeconds = GetComponent<Texture>();
    //}

    // Update is called once per frame
    void Update()
    {
        if (timer > 0)
        {
            timer -= Time.deltaTime;
            if (timer < 0)
            {
                //SceneManager.LoadScene(LevelToLoad);
                onComplete?.Invoke();
                SceneController.LoadScenesAdditive(LevelToLoad);
                if (!keepCurrentScene)
                    SceneManager.UnloadSceneAsync(gameObject.scene.name, UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);
            }
        }

    }
}