﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class LevelGenerator : MonoBehaviour
{
    //List<Area> areas = new List<Area>();
    LevelData level = null;
    public PuzzleController puzzleController;
    public IEnumerator SpawnMap(LevelData l)
    {
        LoadingEvent(0, "Initiliazing Map");
        NavMesh.RemoveAllNavMeshData();
        AreaList tempData = new AreaList();
        level = l;
        Puzzle p = puzzleController.GetPuzzle();
        yield return null;
        LoadingEvent(0.1f, "Spawning Core Path");
        Area roomOne = SpawnRoom(level.startingArea, transform);
        yield return SpawnCorePaths(roomOne, level, true, tempData);
        //LoadingEvent(0.3f, "Closing Paths");
        //yield return CloseAllOpenPaths();
        LoadingEvent(0.4f, "Spawning Core Paths 2");
        yield return SpawnCorePaths(tempData.areas[tempData.areas.Count - 1], level, false, tempData, p.sideRoute);
        bool isPuzzleRoomSpawned = false;
        foreach (Area ad in tempData.areas)
        {
            if (ad.SpawnInfo == p.sideRoute.endRoom)
            {
                isPuzzleRoomSpawned = true;
                break;
            }
        }
        if (!isPuzzleRoomSpawned)
        {
            Debug.LogError("NO PUZZLE ROOM. RELOADING MAP!!! PANIC!!!");
            LevelManager.ReloadScene();
            yield break;
        }

        SpawnDoor(tempData.areas[tempData.areas.Count - 1].pathWays[0], p.doorPrefab, Color.white);
        LoadingEvent(0.8f, "Starting Game");
        CloseAllOpenPaths();
        LoadingEvent(1, "COMPLETE");
        levelGenDone = true;
    }
    public void LoadingEvent(float val, string text)
    {
        Publisher.Raise(new LoadingEvent(text, val));
    }
    public bool levelGenDone;

    SelectSideRouteData ShouldSpawnSideRoom(int roomCount, SideRouteData[] possibleRoutes, Area startPoint)
    {
        List<SelectSideRouteData> validSideRooms = new List<SelectSideRouteData>();
        foreach (SideRouteData srd in possibleRoutes)
        {
            PathWay path;
            int index;
            if (roomCount < srd.minToStart)
                continue;
            if (roomCount > srd.maxToStart)
            {
                LevelManager.ReloadScene();
                return null;
            }
            if (RulesCheck(startPoint, srd.branchingRoom, out path, out index))
                validSideRooms.Add(new SelectSideRouteData(path, index, srd));
        }
        foreach (SelectSideRouteData srd in validSideRooms)
        {
            if (srd.route.occurenceRate == 1)//required room
            {
                return srd;
            }
            else
            {
                float val = Random.Range(0.0f, 1.0f);
                if (val <= srd.route.occurenceRate)
                {
                    return srd;
                }

            }
        }
        return null;

    }
    class SelectSideRouteData
    {
        public PathWay p;
        public int newRoomIndex;
        public SideRouteData route;
        public Area startPoint;
        public SelectSideRouteData(PathWay p, int newRoomIndex, SideRouteData route)
        {
            this.p = p;
            this.newRoomIndex = newRoomIndex;
            this.route = route;
        }
    }
    IEnumerator SpawnSideRoute(SelectSideRouteData ssrd, AreaList output)
    {
        int maxRoom = Random.Range(ssrd.route.minRooms, ssrd.route.maxRooms);
        AreaData lastRoom = ssrd.route.endRoom;

        Debug.Log("Starting Side Path");

        //Spawn STart
        Area branchingRoom = SpawnRoom(ssrd.route.branchingRoom, ssrd.p.transform);
        ssrd.p.ConnectArea(branchingRoom, branchingRoom.pathWays[ssrd.newRoomIndex]);
        ssrd.startPoint = branchingRoom;
        SpawnDoor(branchingRoom.pathWays[ssrd.newRoomIndex], GlobalData.Instance.coreDoor);

        Debug.Log("Starting Side Path");

        yield return SpawnRoutes(GlobalData.Instance.sideDoor, branchingRoom, lastRoom, ssrd.route.possibleRooms, null, maxRoom, maxRoom + 1, true, output);
        Debug.Log("Ending Side Path");
    }

    IEnumerator SpawnRoutes(Color c, Area startPoint, AreaData endRoom, AreaData[] possibleRooms, SideRouteData[] sideRoutes, int minRooms, int maxRooms, bool spawnFinalDoor, AreaList output = null)
    {
        List<Area> spawnedRoomStack = new List<Area>();
        Area lastRoom = startPoint;
        int deleteCheck = 0;
        int globalDelete = 0;
        int maxRoom = Random.Range(minRooms, maxRooms);
        List<SideRouteData> requirendRoutes = new List<SideRouteData>();
        if (sideRoutes != null)
            requirendRoutes = new List<SideRouteData>(sideRoutes);
        bool didSpawnLastRoom = false;
        while (!didSpawnLastRoom)
        {
            if (spawnedRoomStack.Count < maxRoom)
            {
                if (sideRoutes != null)
                {
                    SelectSideRouteData sideRoute = ShouldSpawnSideRoom(spawnedRoomStack.Count, requirendRoutes.ToArray(), lastRoom);
                    if (sideRoute != null)
                    {
                        requirendRoutes.Remove(sideRoute.route);
                        yield return SpawnSideRoute(sideRoute, output);
                        spawnedRoomStack.Add(sideRoute.startPoint);
                        lastRoom = sideRoute.startPoint;
                    }
                    else
                    {
                        /*
                        sideRoute = ShouldSpawnSideRoom(countOffset, l.optionalSideRoutes, ref lastSpawnRoom);
                        if (sideRoute != null)
                        {
                            yield return SpawnSideRoute(sideRoute, lastSpawnRoom);
                        }*/
                    }
                }
            }

            PathWay selectedPath = lastRoom.GetRandomOpenPath();
            if (selectedPath == null)
            {
                selectedPath = lastRoom.GetRandomOpenPath();
            }
            //Get Random Next Room
            AreaData roomToSpawn;
            if (spawnedRoomStack.Count >= maxRoom)
                roomToSpawn = endRoom;
            else
                roomToSpawn = possibleRooms[Random.Range(0, possibleRooms.Length)];

            int pathIndex = roomToSpawn.Area.GetRandomOpenPathIndex();
            if (RulesCheck(lastRoom, selectedPath, roomToSpawn, pathIndex))
            {
                Area spawn = SpawnRoom(roomToSpawn, startPoint.transform);
                PathWay spawnedPath = spawn.pathWays[pathIndex];
                selectedPath.ConnectArea(spawn, spawnedPath);
                if (spawnedRoomStack.Count < maxRoom || spawnFinalDoor)
                    SpawnDoor(spawnedPath, c);

                spawnedRoomStack.Add(spawn);

                lastRoom = spawn;
                deleteCheck = 0;
                if (roomToSpawn == endRoom)
                    didSpawnLastRoom = true;
                yield return new WaitForFixedUpdate();
            }
            else
            {
                deleteCheck++;
                if (deleteCheck > 10)
                {
                    if (spawnedRoomStack.Count < 1)
                    {
                        Debug.LogError("Couldn't place first room. Reloading Map");
                        LevelManager.ReloadScene();
                        yield break;
                    }
                    Debug.LogError("Failed 10 checks - deleting last area");
                    deleteCheck = 0;
                    globalDelete++;
                    Area lr = spawnedRoomStack[spawnedRoomStack.Count - 1];
                    spawnedRoomStack.RemoveAt(spawnedRoomStack.Count - 1);
                    lr.DestoryRoom();
                    if (spawnedRoomStack.Count == 0)
                    {
                        lastRoom = startPoint;
                    }
                    else
                    {
                        lastRoom = spawnedRoomStack[spawnedRoomStack.Count - 1];
                    }
                    if (globalDelete > 10)
                    {
                        Debug.LogError("Deleted 100 rooms. Reloading Map");
                        LevelManager.ReloadScene();
                        yield break;
                    }
                    yield return null;
                }
                else
                {
#if UNITY_EDITOR
                    yield return null;
#endif
                }
            }
        }
        if (output != null)
            output.areas.AddRange(spawnedRoomStack);
        Debug.Log("Ending Core Path");
        yield return null;
    }
    IEnumerator SpawnCorePaths(Area startingRoom, LevelData l, bool sideA, AreaList output, params SideRouteData[] additionalRequiredSideRoutes)
    {
        int maxRoom = sideA ? l.minRoomsToEnd[0] : l.minRoomsToEnd[1];
        int countOffset = sideA ? 0 : l.minRoomsToEnd[0];
        AreaData lastRoom = sideA ? l.midPoint : l.objPoint;

        List<SideRouteData> requiredSideRoutes = new List<SideRouteData>();
        foreach (SideRouteData srd in additionalRequiredSideRoutes)
        {
            if (srd != null)
                requiredSideRoutes.Add(srd);
        }
        foreach (SideRouteData srd in sideA ? l.requiredSideRoutes_1 : l.requiredSideRoutes_2)
        {
            requiredSideRoutes.Add(srd);
        }
        Debug.Log("Starting Core Path");

        yield return SpawnRoutes(GlobalData.Instance.coreDoor, startingRoom, lastRoom, l.possibleRooms, requiredSideRoutes.ToArray(), maxRoom, maxRoom + 1, sideA, output);
        Debug.Log("Ending Core Path");

        yield return null;
    }
    public bool RulesCheck(Area existingRoom, PathWay existingPath, AreaData newRoom, int newPathIndex)
    {
        bool result;
        PathWay newPath = newRoom.Area.pathWays[newPathIndex];

        if (existingRoom.SpawnInfo.invalidNextRooms.Contains(newRoom))
            return false;

        result = existingPath.CanConnectRoom(newRoom.Area, newPath);
        return result;

    }
    public bool RulesCheck(Area existingRoom, AreaData newRoom, out PathWay p, out int newRoomIndex)
    {
        foreach (PathWay existingPath in existingRoom.GetOpenPaths())
        {
            for (int i = 0; i < newRoom.Area.pathWays.Count; i++)
            {
                if (RulesCheck(existingRoom, existingPath, newRoom, i))
                {
                    p = existingPath;
                    newRoomIndex = i;
                    return true;
                }
            }
        }
        p = null;
        newRoomIndex = -1;
        return false;
    }
    public void CloseAllOpenPaths()
    {
        Publisher.Raise(new GameStartEvent(this, level));
    }
    public Area SpawnRoom(AreaData area, Transform root)
    {
        GameObject obj = null;
        // Instantiate room
        obj = Instantiate(area.prefab);
        obj.transform.parent = root;

        obj.transform.position = Vector3.zero;
        obj.transform.rotation = Quaternion.identity;

        obj.GetComponentInChildren<Area>().SpawnInfo = area;
        return obj.GetComponentInChildren<Area>();
    }
    Area SpawnDoor(PathWay spawnPoint, Color c)
    {
        return SpawnDoor(spawnPoint, GlobalData.Instance.doorPrefab, c);
    }
    public Area SpawnDoor(PathWay spawnPoint, GameObject doorPrefab, Color c)
    {
        GameObject obj = null;
        // Instantiate room
        obj = Instantiate(doorPrefab);
        obj.transform.parent = spawnPoint.transform;

        obj.transform.position = spawnPoint.transform.position;
        obj.transform.rotation = Quaternion.identity;
        Area a = obj.GetComponentInChildren<Area>();
        spawnPoint.PositionRoomAtDoorway(a, a.pathWays[0]);
        DoorTransport dt = obj.GetComponentInChildren<DoorTransport>();
        a.SetMapColor(c);
        return a;
    }

}

/*public class SpawnRoomInfo
{
    public AreaData data;
    public Area areaObj;
    public PathWay usedPathway;
    public PathWay nextPathway;

    public SpawnRoomInfo()
    {
    }

    public SpawnRoomInfo(AreaData data, Area areaObj, PathWay usedPathway, PathWay nextPathway)
    {
        this.data = data;
        this.areaObj = areaObj;
        this.usedPathway = usedPathway;
        this.nextPathway = nextPathway;
    }
    public void Set(AreaData data, Area areaObj, PathWay usedPathway, PathWay nextPathway)
    {
        this.data = data;
        this.usedPathway = usedPathway;
        this.nextPathway = nextPathway;
    }
    public SpawnRoomInfo Set(SpawnRoomInfo spi)
    {
        this.data = spi.data;
        this.areaObj = spi.areaObj;
        this.usedPathway = spi.usedPathway;
        this.nextPathway = spi.nextPathway;
        return this;
    }
}*/

public class SurrogateArea : Area
{
    public AreaData surrogate;
    protected override void Awake()
    {

    }

    public void Set(AreaData a)
    {
        surrogate = a;
        for (int i = 0; i < pathWays.Count; i++)
        {
            if (a.Area.pathWays.Count < i)
            {
                pathWays[i].transform.localPosition = surrogate.Area.pathWays[i].transform.localPosition;
                pathWays[i].transform.localRotation = surrogate.Area.pathWays[i].transform.localRotation;
            }
            else
            {
                pathWays[i].gameObject.SetActive(false);
            }
        }
        boundsCol.center = surrogate.Area.boundsCol.center;
        boundsCol.size = surrogate.Area.boundsCol.size;
        boundsCol.transform.localPosition = surrogate.Area.boundsCol.transform.localPosition;
        boundsCol.transform.localRotation = surrogate.Area.boundsCol.transform.localRotation;
        OnValidate();
    }
    public void Spawn()
    {
        //Spawn surrogate data
        //Destroy Self
    }
}
public class AreaList
{
    public List<Area> areas = new List<Area>();
}