﻿using UnityEngine;
using System.Collections;
using UnityEngine.AI;

public class BasicAI : MonoBehaviour, ISpawnable
{
    protected Transform targetPos = null;
    protected AIState aiState;
    public Animator anim;
    public CustomAudioClip spottedSFX, deathSFX;
    public bool ignoreY = true;
    public NavMeshAgent agent;
    public Collider col;
    Spawner spawner;
    public enum AIState
    {
        IDLE,
        PURSUE,
        ATTACK,
        DEAD,
        DOWNED
    }
    public virtual void Awake()
    {
    }
    protected virtual void OnEnable()
    {
        if (dead)
            anim.Play("Death", -1, 1);
    }
    protected virtual void OnDisable()
    {
        if (!dead)
        {
            SetState(AIState.IDLE);
            if (spawner)
                transform.position = spawner.transform.position;
        }
    }
    private void Update()
    {
        Vector3 targetPosition;
        float distance;
        switch (aiState)
        {
            case AIState.IDLE:
                break;
            case AIState.PURSUE:
                targetPosition = targetPos.position;
                if (ignoreY)
                    targetPosition.y = transform.position.y;
                distance = Vector3.Distance(targetPosition, transform.position);
                if (distance <= attackRange)
                {
                    SetState(AIState.ATTACK);
                    return;
                }
                MoveTowards(targetPosition);
                break;
            case AIState.ATTACK:
                break;
            default:
                break;
        }
    }
    public void MoveTowards(Vector3 targetPos)
    {
        agent.SetDestination(targetPos);
    }
    private void OnTriggerEnter(Collider other)
    {
        if ((aiState == AIState.IDLE) && other.tag == "Player")
        {
            spottedSFX.Play();
            targetPos = other.transform;
            SetState(AIState.PURSUE);
        }
    }
    bool dead = false;
    public virtual void SetState(AIState state)
    {
        aiState = state;
        switch (aiState)
        {
            case AIState.IDLE:
                if (anim)
                    anim.SetBool("isRunning", false);
                if (agent.isOnNavMesh)
                    agent.SetDestination(transform.position);
                break;
            case AIState.PURSUE:
                if (anim)
                    anim.SetBool("isRunning", true);

                break;
            case AIState.ATTACK:
                if (anim)
                {
                    anim.SetBool("isRunning", false);
                    anim.SetTrigger("shoot");
                }
                agent.SetDestination(transform.position);
                StartCoroutine(AttackAction());
                break;
            case AIState.DEAD:
                StopAllCoroutines();
                dead = true;
                anim.SetTrigger("death");
                //agent.SetDestination(transform.position);
                break;
            case AIState.DOWNED:
                StopAllCoroutines();
                if (anim)
                    anim.SetTrigger("down");
                agent.SetDestination(transform.position);
                break;
            default:
                break;
        }
    }
    public void DetectOnHit()
    {
        if ((aiState == AIState.IDLE))
        {
            spottedSFX.Play();
            targetPos = Player.Instance.transform;
            SetState(AIState.PURSUE);
        }
    }
    public float attackTime;
    public float windUpTime;
    public float attackRange;
    public bool canAttackCancel = true;
    IEnumerator AttackAction()
    {
        agent.SetDestination(transform.position);
        //agent.updateRotation = false;
        Vector3 targetPosition;

        float t = windUpTime;
        float rotateTime = windUpTime / 3;
        while (t > 0)
        {
            t -= Time.deltaTime;
            if (t > rotateTime)
                RotateTowards(targetPos.position);
            yield return null;
        }
        targetPosition = targetPos.position;
        // yield return new WaitForSeconds(windUpTime);

        if (ignoreY)
            targetPosition.y = transform.position.y;
        else
            targetPosition.y += 0.5f;
        float distance = Vector3.Distance(targetPosition, transform.position);
        if (distance > attackRange && canAttackCancel)
        {
            anim.SetTrigger("cancel");
            agent.updateRotation = true;
            SetState(AIState.PURSUE);
            yield break;
        }

        Attack();
        yield return new WaitForSeconds(attackTime);
        AttackEnd();
        targetPosition = targetPos.position;
        if (ignoreY)
            targetPosition.y = transform.position.y;
        else
            targetPosition.y += 0.5f;
        distance = Vector3.Distance(targetPosition, transform.position);
        if (LevelManager.gameOver)
        {
            agent.updateRotation = true;
            SetState(AIState.IDLE);
        }
        else if (distance <= attackRange)
        {
            Vector3 fwd = targetPosition - transform.position;
            transform.forward = Vector3.Slerp(transform.forward, fwd, smoothRotSpeed);
            SetState(AIState.ATTACK);
        }
        else
        {
            agent.updateRotation = true;
            SetState(AIState.PURSUE);
        }
    }
    private void RotateTowards(Vector3 target)
    {
        Vector3 direction = (target - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));    // flattens the vector3
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 10);
    }
    public float smoothRotSpeed = 1;
    public virtual void Attack()
    {

    }
    public virtual void AttackEnd()
    {

    }
    public virtual void OnDie()
    {
        col.enabled = false;
        agent.enabled = false;
        deathSFX.Play();
        SetState(AIState.DEAD);
        WinMenu.AddStat("kills", 1);
    }
    public void OnSpawn(Spawner s)
    {
        spawner = s;
    }
}

