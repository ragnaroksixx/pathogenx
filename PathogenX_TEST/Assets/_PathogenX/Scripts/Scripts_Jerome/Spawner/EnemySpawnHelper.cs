﻿using UnityEngine;
using System.Collections;

public class EnemySpawnHelper : SpawnHelper
{
    public static int numRoomsTraversed = 0;
    public override float GetSpawnMultiplier(SpawnData sd)
    {
        float val = base.GetSpawnMultiplier(sd);
        BasicAI enemy = sd.obj.GetComponentInChildren<BasicAI>();
        if (enemy)
        {
            val = Mathf.Sqrt(numRoomsTraversed) * 0.5f;
        }
        return val;
    }
}
