﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Julhiecio TPS Controller/Tools/Comment")]
public class CommentTool : MonoBehaviour
{
    [TextArea(3, 300)]
    public string Comment;
}
