﻿using UnityEngine;
using System.Collections;
using System;

public class MapCamera : MonoBehaviour
{
    public static MapCamera Instance;
    Vector3 resetPos;
    private void Awake()
    {
        Instance = this;
        Publisher.Subscribe<AreaEnterEvent>(OnAreaEnter);
    }
    private void OnDestroy()
    {
        Publisher.Unsubscribe<AreaEnterEvent>(OnAreaEnter);
    }

    private void OnAreaEnter(AreaEnterEvent e)
    {
        Vector3 pos = e.area.mapImage.transform.position;
        pos.y = transform.position.y;
        transform.position = pos;
        resetPos = pos;
    }
    public void ResetPos()
    {
        transform.position = resetPos;
    }
}
