﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class LevelManager : MonoBehaviour
{
    public LevelGenerator levelGen;
    public GameObject cameraSystem;
    GameObject player;
    public static LevelManager Instance;
    public static bool gameOver;
    public static bool zoneStarted = false;
    public static bool RestartingRound = false;
    private void Awake()
    {
        Instance = this;
        gameOver = false;
        RestartingRound = false;
    }

    IEnumerator Start()
    {
        Publisher.Raise(new ClearAllStatsEvent());
        AudioListener al = gameObject.AddComponent<AudioListener>();
        yield return levelGen.SpawnMap(Global.Instance.currentGameInfo);
        Destroy(al);
        player = GameObject.Instantiate(Global.Instance.currentGameInfo.player, transform);
        GameObject.Instantiate(cameraSystem, transform);
        Inventory.Instance.player = player.GetComponent<ThirdPersonController>();
        Global.Instance.currentGameInfo.bgm.Play();
        startTime = Time.time;
        EnemySpawnHelper.numRoomsTraversed = 0;
    }
    public static void GameOver()
    {
        gameOver = true;
        Instance.StartCoroutine(Instance.GameOverSequence());
    }
    public static void ReloadScene()
    {
        Debug.LogError("Reloaded the scene");
        Time.timeScale = 1;
        RestartingRound = true;
        zoneStarted = false;
        SceneController.ReloadLevelScene();
    }
    IEnumerator GameOverSequence()
    {
        Time.timeScale = 1;
        yield return new WaitForSeconds(2);
        Publisher.Raise(new GameOverEvent());
        AudioSystem.PlayBGM(null, Vector3.zero);
        //NextLevel();
    }

    public GameObject Player { get => player; set => player = value; }

    public static void EndLevel()
    {
        Instance.player.SetActive(false);
        int seconds = Mathf.FloorToInt(Time.time - Instance.startTime);
        WinMenu.AddStat("time", seconds);
        int parTime = 540;
        int timeScore = Mathf.Max(0, parTime - seconds) * 100;
        WinMenu.AddStat("score", timeScore);
    }
    float startTime;
}

