﻿using UnityEngine;
using System.Collections;
using Sirenix.OdinInspector;
#if UNITY_EDITOR
using UnityEditor.SceneManagement;
using UnityEditor;
#endif
//[CreateAssetMenu(menuName = "Global")]
public class GlobalData : ScriptableObject
{
    public Sprite transparentImage;
    public GameObject areaDataEditorPrefab;
    public GameObject fixedCameraPrefab;
    public GameObject doorPrefab;
    public Color coreDoor, lockedDoor, sideDoor;
    protected static GlobalData instance;
    public static GlobalData Instance
    {
        get
        {
            if (instance == null)
                instance = Resources.Load<GlobalData>("GlobalData");
            return instance;
        }
    }

}
#if UNITY_EDITOR
public class EditorGlobal

{
    [MenuItem("Spawn AreaData", menuItem = "PathogenX Tools/Spawn AreaData")]
    public static void SpawnAreaData()
    {
        GameObject g = UnityEditor.PrefabUtility.InstantiatePrefab(GlobalData.Instance.areaDataEditorPrefab) as GameObject;
        StageUtility.PlaceGameObjectInCurrentStage(g);
        Area a = g.GetComponentInChildren<Area>();
        a.OnValidate();
    }

    [MenuItem("Spawn FixedCamera", menuItem = "PathogenX Tools/Spawn FixedCamera")]
    public static void SpawnFixedCamera()
    {
        GameObject g = UnityEditor.PrefabUtility.InstantiatePrefab(GlobalData.Instance.fixedCameraPrefab) as GameObject;
        StageUtility.PlaceGameObjectInCurrentStage(g);
    }
}
#endif

