﻿using UnityEngine;
using System.Collections;

public class StartMenu : Menu
{
    public GameObject assets;
    public void NewGame()
    {
        SceneController.LoadScenesAdditive("Ralf_Cutscene_MissionBriefing");
        Hide(true);
    }
    public override void Show(bool instant = false)
    {
        base.Show(instant);
        assets.SetActive(true);
    }
    public override void Hide(bool instant = false)
    {
        base.Hide(instant);
        assets.SetActive(false);
    }
    public void Credits()
    {

    }
    public void Quit()
    {
        Application.Quit();
    }
}
