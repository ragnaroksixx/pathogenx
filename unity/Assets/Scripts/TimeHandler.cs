﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;
using System;

public class TimeHandler : MonoBehaviour
{
    CoroutineHandler coroutine;
    public static TimeHandler Instance;
    Tween t;
    public bool inputLock;
    private void Awake()
    {
        Instance = this;
        isPaused = false;
        Time.timeScale = 1;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }
    // Use this for initialization
    void Start()
    {
        coroutine = new CoroutineHandler(this);
    }

    // Update is called once per frame
    void Update()
    {
        if (!LevelManager.Instance)
            return;
        if (LevelManager.gameOver)
            return;
        if (LevelManager.Instance.levelGen.levelGenDone)
        {
            if (!softPause && Input.GetKeyDown(KeyCode.Tab) && !inputLock)
            {
                if (isPaused)
                    UnPause();
                else
                    Pause();
            }
        }

        if (Input.GetKeyDown(KeyCode.M))
        {
            if (AudioSystem.Instance.MASTERvolumePercentage == 0)
            {
                AudioSystem.SetMasterPercentage(25);
            }
            else
            {
                AudioSystem.SetMasterPercentage(0);
            }
        }

    }
    public bool isPaused = false;
    public void Pause()
    {
        coroutine.StopCoroutine();
        Time.timeScale = 0;
        isPaused = true;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        Publisher.Raise(new PauseEvent(true));
    }
    public void UnPause()
    {
        isPaused = false;
        Time.timeScale = 1;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        Publisher.Raise(new PauseEvent(false));
    }

    public void SlowTime(float time)
    {
        coroutine.StartCoroutine(SlowMo(time), OnInterrupt);
    }
    IEnumerator SlowMo(float time)
    {
        float declTime = time / 2;
        t = DOTween.To(() => Time.timeScale, x => Time.timeScale = x, 0.75f, declTime);
        yield return new WaitForSeconds(time / 2);
        t = DOTween.To(() => Time.timeScale, x => Time.timeScale = x, 1, declTime);
        Time.timeScale = 1;
    }
    void OnInterrupt()
    {
        t?.Kill();
        Time.timeScale = 1;
    }

    internal void SoftPause()
    {
        coroutine.StopCoroutine();
        Time.timeScale = 0;
        isPaused = true;
        softPause = true;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    public void SoftUnpause()
    {
        isPaused = false;
        softPause = false;
        Time.timeScale = 1;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    bool softPause;
}
