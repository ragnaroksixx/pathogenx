﻿using UnityEngine;
using System.Collections;

public class AreaTriggers : Interactable
{
    protected Area area;
    Spawner[] spawners;
    public virtual void Init(Area a)
    {
        area = a;
        Publisher.Subscribe<AreaEnterEvent>(OnAreaExit);
        spawners = transform.parent.GetComponentsInChildren<Spawner>();
    }
    protected virtual void OnDestroy()
    {
        Publisher.Unsubscribe<AreaEnterEvent>(OnAreaExit);
    }
    public virtual void OnAreaEnter()
    {
        area.Show();
        Publisher.Raise(new AreaEnterEvent(area, area.SpawnInfo));
        foreach (Spawner s in spawners)
        {
            s.Spawn();
        }
    }
    public virtual void OnAreaExit(AreaEnterEvent aee)
    {
        if (aee == null || aee.area != area)
            area.Hide();
    }

    public override void OnEnter(Collider other)
    {
        OnAreaEnter();
    }

    public override void OnExit(Collider other)
    {
        OnAreaExit(null);
    }
}
