﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Inventory : MonoBehaviour
{
    [HideInInspector]
    public List<Item> inventoryItems;
    public ThirdPersonController player;
    public static Inventory Instance;
    private void Awake()
    {
        Instance = this;
        inventoryItems = new List<Item>(GetComponentsInChildren<Item>());
    }
    private void OnDestroy()
    {
        Instance = null;
    }

    public List<Item> GetInventory()
    {
        List<Item> results = new List<Item>();
        foreach (Item i in inventoryItems)
        {
            if (i.GetCount() > 0)
                results.Add(i);
        }
        return results;
    }
    public bool HasItem(string itemName)
    {
        foreach (Item i in inventoryItems)
        {
            if (i.displayName.ToLower() == itemName.ToLower())
                return i.GetCount() > 0;
        }
        return false;
    }
    public Item GetItem(string itemName)
    {
        foreach (Item i in inventoryItems)
        {
            if (i.displayName.ToLower() == itemName.ToLower())
                return i;
        }
        return null;
    }
    public void Add(string itemName, int amount = 1)
    {
        foreach (Item i in inventoryItems)
        {
            if (i.displayName.ToLower() == itemName.ToLower())
            {
                i.qty += amount;
                WinMenu.AddStat("items", 1);
                WinMenu.AddStat("score", 50);
                return;
            }
        }
    }
    public void Remove(string itemName, int amount = 1)
    {
        foreach (Item i in inventoryItems)
        {
            if (i.displayName.ToLower() == itemName.ToLower())
            {
                i.qty -= amount;
                return;
            }
        }
    }
    public bool IsFull()
    {
        int maxInv = 8;
        foreach (Item i in inventoryItems)
        {
            if (i.GetCount() > 0)
                maxInv--;
            if (maxInv <= 0)
                return true;
        }

        return false;
    }
}
