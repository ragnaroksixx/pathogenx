﻿using UnityEngine;
using UnityEditor;

[AddComponentMenu("Julhiecio TPS Controller /Physics/Vehicle")]
[RequireComponent(typeof(Rigidbody))]
public class Vehicle : MonoBehaviour
{
	ThirdPersonController pl;//player

	[Header("Vehicle Settings")]
	public bool IsMobile;

	private ButtonVirtual LeftButton,RightButton,BackButon,ForwardButton, BreakButton;
	[HideInInspector] private int MobileInputHorizontal;
	[HideInInspector] private int MobileInputVertical;
	[HideInInspector] private int MobileInputBreak;
	private float _horizontal;
	private float _vertical;
	private bool _break;

	public VehicleType TypeOfVehicle;
	public GameObject SteeringWheel;
	public float MaxVelocity = 60;
	public float TorqueForce = 2000;
	public float BreakForce = 8000;
	public float Friction = 200;
	public float AntiRoll = 50000;
	public float WheelRotationVelocity = 5f;
	public float WheelAngleMax = 45;

	[Header("Motorcycle Settings")]
	public float MotocycleAngleMax = 30;
	public Transform MotorcycleBodyPivot;
	public Transform MotorcycleRotZ;


	[HideInInspector] float RotZ;
	[HideInInspector] float SmothRotZ;
	[HideInInspector] float SmothSteeringWheel;
	[HideInInspector] float smoothYrotation;


	[Header("IK and Player Settings")]
	public Transform PlayerLocation;
	public Transform LeftHandPositionIK, RightHandPositionIK, LeftFootPositionIK, RightFootPositionIK;

	[Header("Physics Settings")]
	public bool AirControl;
	public float InAirForce;
	public LayerMask WhatIsGround;
	public Transform CenterOfMassPosition;
	public WheelCollider[] WheelColliders;
	public Transform[] Wheels;

	public bool KillPlayerWhenHitsTooHard;
	public float VelocityToKillPLayer = 30;

	[HideInInspector]public Rigidbody rb;
	private RaycastHit hit;

	[Header("Overturned Check")]
	public bool CheckOverturned;
	public Vector3 OverturnedCheckOfsset;
	public Vector3 OverturnedCheckScale;

	public bool IsOverturned;




	[Header("Vehicle Stats")]
	public bool IsOn;
	public bool OnFloor;
	public bool ControllRotation;

	[Header("Mesh Collider Correction")]
	public GameObject VehicleMesh;
	public MeshCollider MeshColliderCorrect;

	public enum VehicleType{
		Car,
		Motorcycle,
	}

    void Awake()
    {
		pl = FindObjectOfType<ThirdPersonController>();
		if (MeshColliderCorrect != null)
		{
			MeshColliderCorrect.transform.SetParent(null);
		}
		rb = GetComponent<Rigidbody> ();
		rb.centerOfMass = CenterOfMassPosition.localPosition;
		if(TypeOfVehicle == VehicleType.Motorcycle && MotorcycleBodyPivot != null)
        {
			MotorcycleBodyPivot.SetParent(null);
        }
        if (IsMobile)
        {
			LeftButton = GameObject.Find("LeftButton").GetComponent<ButtonVirtual>();
			RightButton = GameObject.Find("RightButton").GetComponent<ButtonVirtual>();
			BackButon = GameObject.Find("BackButton").GetComponent<ButtonVirtual>();
			ForwardButton = GameObject.Find("ForwardButton").GetComponent<ButtonVirtual>();
			BreakButton = GameObject.Find("BreakButton").GetComponent<ButtonVirtual>();
		}
	}
	void FixedUpdate()
    {
		UpdateMeshColliderCorrection();

		//---MOTORCYCLE VEHICLE TYPE---
		if (TypeOfVehicle == VehicleType.Motorcycle) {
			//If vehicle is on you can controll
			ControllMotorBike ();
			VehicleControl();
		}
		//-------CAR VEHICLE TYPE------
		if (TypeOfVehicle == VehicleType.Car) {
			AntiOverturned ();
			VehicleControl();
		}

	}
    private void Update()
    {

		//Ground Check
		if (Physics.Raycast(CenterOfMassPosition.position, -transform.up, out hit, 5, WhatIsGround))
		{
			OnFloor = true;
			if(hit.collider.tag == "Loop")
            {
				ControllRotation = false;
            }
            else
            {
				ControllRotation = true;
			}
		}
		else
		{
			ControllRotation = true;
			transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.FromToRotation(transform.up, Vector3.up) * transform.rotation, 0.2f * Time.deltaTime);
			OnFloor = false;
		}

		//Overturned check and correction
		Overturned();
		//Input Controller (Mobile and PC)
		InputController();
	}
	public void VehicleControl(){
		Vector3 pos;
		Quaternion rot;
		if (rb.velocity.magnitude > MaxVelocity)
		{
			var rbvel = rb.velocity;
			rbvel.y = rb.velocity.y;
			rb.velocity = Vector3.ClampMagnitude(rbvel, MaxVelocity);
		}

		for (int i = 0; i < WheelColliders.Length; i++)
		{
			//Update wheels 
			WheelColliders [i].GetWorldPose (out pos, out rot);
			Wheels[i].position = pos;
			Wheels [i].rotation = Quaternion.Lerp(Wheels[i].rotation, rot, 15 * Time.deltaTime);

			if (IsOn == true) {
				WheelColliders [i].brakeTorque = _break ? BreakForce : Friction - Mathf.Abs (_vertical * Friction);

				//If is a car
				if (TypeOfVehicle == VehicleType.Car){
					if (i < 2) {
						WheelColliders [i].steerAngle = Mathf.Lerp (WheelColliders [i].steerAngle, WheelAngleMax * _horizontal, WheelRotationVelocity * Time.deltaTime);
					}
					WheelColliders [i].motorTorque = _vertical * TorqueForce;
				}


				//If is a motorcycle
				if (TypeOfVehicle == VehicleType.Motorcycle){

					if (i == 0) {
						WheelColliders [0].steerAngle = Mathf.Lerp (WheelColliders [i].steerAngle, WheelAngleMax * _horizontal, WheelRotationVelocity * Time.deltaTime);
					}
					WheelColliders[i].motorTorque = _vertical * TorqueForce;
				}

				if (SteeringWheel != null)
				{
					SmothSteeringWheel = Mathf.Lerp(SmothSteeringWheel, _horizontal * WheelAngleMax, WheelRotationVelocity * Time.deltaTime);
					SteeringWheel.transform.localEulerAngles = new Vector3(SteeringWheel.transform.localEulerAngles.x, SmothSteeringWheel, SteeringWheel.transform.localEulerAngles.x);
				}
            }
            else
            {
				WheelColliders[i].brakeTorque = BreakForce / 4;
			}
		}

        if (AirControl)
        {
			rb.AddTorque(transform.up * _horizontal * 100 * InAirForce / 4f);
			rb.AddTorque(transform.right * _vertical * 100* InAirForce);
		}
	}
	public void ControllMotorBike()
	{
		if (IsOn)
		{
			if (ControllRotation)
			{
				if (_vertical > 0 && _horizontal != 0f)
				{
					RotZ = Mathf.LerpAngle(RotZ, rb.velocity.normalized.magnitude * -_horizontal * MotocycleAngleMax, 5f * Time.deltaTime);
				}
				else
				{
					if (_vertical < 0)
					{
						RotZ = Mathf.LerpAngle(RotZ, rb.velocity.normalized.magnitude * -_horizontal * MotocycleAngleMax / 2, 3f * Time.deltaTime);
					}
					else
					{
						RotZ = Mathf.LerpAngle(RotZ, 0, 3f * Time.deltaTime);
					}
				}
            }
            else
            {
				RotZ = 0;
            }
			
				var rot = MotorcycleRotZ.localEulerAngles;
				rot.x = 0;
				rot.y = 0;
				rot.z = RotZ;

				MotorcycleRotZ.localEulerAngles = new Vector3(0, 0,RotZ);
				if (OnFloor)
				{
					MotorcycleBodyPivot.rotation = Quaternion.Slerp(MotorcycleBodyPivot.rotation, Quaternion.FromToRotation(MotorcycleBodyPivot.up, hit.normal) * MotorcycleBodyPivot.rotation, 5 * Time.deltaTime);
					MotorcycleBodyPivot.position = hit.point;
				}
				else
				{
					MotorcycleBodyPivot.rotation = Quaternion.Slerp(MotorcycleBodyPivot.rotation, Quaternion.FromToRotation(MotorcycleBodyPivot.up, Vector3.up) * MotorcycleBodyPivot.rotation, 0.8f * Time.deltaTime);
					MotorcycleBodyPivot.position = transform.position;
				}
				MotorcycleBodyPivot.eulerAngles = new Vector3(MotorcycleBodyPivot.eulerAngles.x, transform.eulerAngles.y, MotorcycleBodyPivot.eulerAngles.z);
				var motorcyclerot = MotorcycleBodyPivot.eulerAngles;
				motorcyclerot.z = MotorcycleRotZ.localEulerAngles.z;

				transform.rotation = Quaternion.Slerp(transform.rotation, MotorcycleRotZ.rotation, 3 * Time.deltaTime);
			
			//If is on floor -> freeze rotation
			if (OnFloor)
			{
				rb.angularDrag = 11;
				rb.constraints = RigidbodyConstraints.FreezeRotationX;
				rb.constraints = RigidbodyConstraints.FreezeRotationZ;
			}
			else
			{
				rb.angularDrag = 1f;
				rb.constraints = RigidbodyConstraints.None;
			}
		}
	}
	public void AntiOverturned(){

		WheelHit hit;
		var travelL = 1.0;
		var travelR = 1.0;

		var groundedL = WheelColliders[1].GetGroundHit(out hit);
		if (groundedL)
			travelL = (-WheelColliders[1].transform.InverseTransformPoint(hit.point).y - WheelColliders[1].radius) / WheelColliders[1].suspensionDistance;

		var groundedR = WheelColliders[0].GetGroundHit(out hit);
		if (groundedR)
			travelR = (-WheelColliders[0].transform.InverseTransformPoint(hit.point).y - WheelColliders[0].radius) / WheelColliders[0].suspensionDistance;

		var antiRollForceDouble = (travelL - travelR) * AntiRoll;
		float antiRollForce = (float)antiRollForceDouble;
		if (groundedL)
			rb.AddForceAtPosition(WheelColliders[1].transform.up * -antiRollForce,
				WheelColliders[1].transform.position);  
		if (groundedR)
			rb.AddForceAtPosition(WheelColliders[0].transform.up * antiRollForce,
				WheelColliders[0].transform.position);  
		
	}	
	public void InputController()
    {
		_horizontal = Input.GetAxisRaw("Horizontal") + MobileInputHorizontal;
		_vertical = Input.GetAxisRaw("Vertical") + MobileInputVertical;
		if (Input.GetButton("Jump") == true)
		{
			_break = Input.GetButton("Jump");
        }
        else if(IsMobile)
        {
			_break = BreakButton.IsPressed;
		}

		if (IsMobile)
        {
            //Forward, Backward
            if (ForwardButton.IsPressed)
            {
				MobileInputVertical = 1;
            }
			if (BackButon.IsPressed)
			{
				MobileInputVertical = -1;
			}
			if(BackButon.IsPressed == false && ForwardButton.IsPressed == false)
            {
				MobileInputVertical = 0;
			}
			//Right, Left
			if (RightButton.IsPressed)
			{
				MobileInputHorizontal = 1;
			}
			if (LeftButton.IsPressed)
			{
				MobileInputHorizontal = -1;
			}
			if (LeftButton.IsPressed == false && RightButton.IsPressed == false)
			{
				MobileInputHorizontal = 0;
			}
		}
	}
	public void UpdateMeshColliderCorrection()
	{
		if (MeshColliderCorrect != null && VehicleMesh != null)
		{
			MeshColliderCorrect.transform.position = VehicleMesh.transform.position;
			MeshColliderCorrect.transform.rotation = VehicleMesh.transform.rotation;
		}
	}
	public void GenerateMeshColliderCorrection()
    {
		if(VehicleMesh != null)
        {
			if (MeshColliderCorrect == null)
			{
				var newmeshcollider = (GameObject)Instantiate(VehicleMesh, VehicleMesh.transform.position, VehicleMesh.transform.rotation);

				//destroy existing collider
				if (newmeshcollider.TryGetComponent(out Collider collider) != false)
				{
					DestroyImmediate(collider.GetComponent<Collider>());
				}
				//add mesh collider
				newmeshcollider.AddComponent<MeshCollider>();

				//set mesh collider
				newmeshcollider.GetComponent<MeshCollider>().convex = false;
				newmeshcollider.GetComponent<MeshCollider>().sharedMesh = VehicleMesh.GetComponent<MeshFilter>().sharedMesh;
				MeshColliderCorrect = newmeshcollider.GetComponent<MeshCollider>();

				//add mesh collider correct script and set mesh
				newmeshcollider.AddComponent<MeshColliderCorrect>();
				newmeshcollider.GetComponent<MeshColliderCorrect>().Mesh = VehicleMesh;

				//set new mesh collider properties
				newmeshcollider.name = "[Mesh Collider Correct]";
				newmeshcollider.transform.SetParent(this.transform);
				newmeshcollider.layer = 12; //Vehicle Mesh Collider

				Debug.Log("MeshColliderGenerate: meshcollider successfully generated.");
				Debug.Log("MeshColliderGenerate: unity may give an error on the console at the beginning of the game, but it will not affect anything...");

				//Destroy mesh renderer
				if (newmeshcollider.TryGetComponent(out MeshFilter meshfilter) != false)
				{
					DestroyImmediate(meshfilter.GetComponent<MeshRenderer>());
					DestroyImmediate(meshfilter.GetComponent<MeshFilter>());
				}

			}
			else
            {
				Debug.LogWarning("MeshColliderGenerate: there is already a meshcollider, if you want another one, delete the existing one or remove it from the inspector");
			}
		}
        else
        {
			Debug.LogError("MeshColliderGenerate: need a linked mesh");
        }
    }
	public void Overturned()
	{
		if (CheckOverturned == true)
		{
			var overturned_check = Physics.OverlapBox(transform.position + transform.up * OverturnedCheckOfsset.y 
				+ transform.right * OverturnedCheckOfsset.x + transform.forward * OverturnedCheckOfsset.z,
				OverturnedCheckScale, transform.rotation, WhatIsGround);

			if (transform.eulerAngles.z > 45 || transform.eulerAngles.z < -45)
			{
				if (overturned_check.Length != 0)
				{
					IsOverturned = true;
				}
            }
            else
            {
				IsOverturned = false;
            }

			if (IsOverturned == true)
			{
				/*var rot = transform.eulerAngles;
				rot.z = Mathf.Lerp(rot.z, 0, 10 * Time.deltaTime);
				transform.eulerAngles = rot;*/
				transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.FromToRotation(transform.up, Vector3.up) * transform.rotation, 0.3f * Time.deltaTime);
			}
		}
	}
    private void OnCollisionEnter(Collision collision)
    {
		if (IsOn == false)
			return;
        if(transform.InverseTransformDirection(rb.velocity).z > VelocityToKillPLayer && KillPlayerWhenHitsTooHard)
        {
			pl.Life = 0;
        }
    }
	private void OnDrawGizmos()
	{
		if (CheckOverturned)
			Gizmos.color = Color.white;
			Gizmos.DrawWireCube(transform.position + transform.up * OverturnedCheckOfsset.y + transform.right * OverturnedCheckOfsset.x + transform.forward * OverturnedCheckOfsset.z, OverturnedCheckScale);
	}
}
#if UNITY_EDITOR
[CustomEditor(typeof(Vehicle))]
public class VehicleEditor : Editor
{
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
		Vehicle vehicle = (Vehicle)target;
		if (GUILayout.Button("Generate Mesh Collider Correct"))
		{
			vehicle.GenerateMeshColliderCorrection();
		}
	}
}
#endif
