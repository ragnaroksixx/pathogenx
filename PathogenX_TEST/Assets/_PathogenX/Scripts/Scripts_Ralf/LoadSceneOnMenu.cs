﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class LoadSceneOnMenu : MonoBehaviour
{
    //public GameObject blackOutSquare;

    public string SceneName = "";

    public Image black;
    public Animator anim;

    //void OnTriggerEnter(Collider other)
    //{
    //    if (other.tag == "Player")
    //    {
    //        StartCoroutine(Fading());
    //        //SceneManager.LoadScene(SceneName); // loads scene When player enter the trigger collider
    //    }
    //}

    private void Update()
    {
        if (EventSystem.current.currentSelectedGameObject == gameObject
            && Input.GetMouseButtonDown(0))
        {
            LoadTargetScene();
        }
    }

    public void LoadTargetScene()
    {
        StartCoroutine(Fading());
    }

    IEnumerator Fading()
    {
        anim.SetBool("Fade", true);
        yield return new WaitUntil(() => black.color.a == 1);
        SceneManager.LoadScene(SceneName);
    }

    //void OnTriggerStay(Collider other)
    //{
    //    if (other.tag == "Player")
    //    {
    //        if (Input.GetKeyDown(KeyCode.E)) // loads scene on user input when in collider
    //        {
    //            SceneManager.LoadScene(SceneName);
    //        }

    //        if (Input.GetKey("joystick button 2")) // loads scene on user input when in collider
    //        {
    //            SceneManager.LoadScene(SceneName);
    //        }
    //    }
    //}

    //void Update()
    //{

    //}
}