﻿using UnityEngine;
using System.Collections;

public class WeaponPickUp : SimpleKeyInteractable
{
    public Weapon.WeaponType weaponType;
    public int ammo;
    public RPGTalkArea RPGTalkArea;
    private void Awake()
    {
        Destroy(GetComponentInChildren<Weapon>());
    }
    public override void OnPress()
    {
        for (int i = 0; i < Inventory.Instance.player.Weapons.Length; i++)
        {
            if (Inventory.Instance.player.Weapons[i].Type == weaponType)
            {
                if (Inventory.Instance.player.Weapons[i].Unlocked)
                {
                    Inventory.Instance.player.Weapons[i].TotalBullets +=
                        Inventory.Instance.player.Weapons[i].BulletsPerMagazine;
                    Inventory.Instance.player.Reload(Inventory.Instance.player.Weapons[i]);
                }
                else
                {
                    if (Inventory.Instance.IsFull())
                    {
                        Publisher.Raise(new NotificationEvent("<color=red>Inventory Full"));
                        return;
                    }
                    RPGTalkArea?.StartTalk();
                    Inventory.Instance.player.Weapons[i].Unlocked = true;
                    Inventory.Instance.player.Weapons[i].BulletsAmounts =
                        Inventory.Instance.player.Weapons[i].BulletsPerMagazine;
                    if (Inventory.Instance.player.WeaponInUse == null)
                        Inventory.Instance.player.Equip(Inventory.Instance.player.Weapons[i].WeaponID);
                }
                WinMenu.AddStat("items", 1);
                WinMenu.AddStat("score", 50);
                Publisher.Raise(new HideNotificationEvent());
                GameObject c1 = transform.GetChild(0).gameObject;
                GameObject c2 = transform.GetChild(1).gameObject;
                Destroy(c1);
                Destroy(c2);
                Destroy(GetComponent<Animator>());
                Destroy(GetComponent<Collider>());
                enabled = false;
            }
        }
    }
    public override string GetText()
    {
        return base.GetText() + " [" + keyPress.ToString() + "]"; ;
    }
}
