﻿using UnityEngine;
using System.Collections;

public class SimpleBillboard : MonoBehaviour
{
    Vector3 fwd;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Player.Instance)
        {
            fwd = -Player.Instance.transform.position + transform.position;
            fwd.y = 0;
            transform.forward = fwd;
        }
    }
}
