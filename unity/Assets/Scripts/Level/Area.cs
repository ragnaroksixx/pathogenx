﻿using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(BoxCollider))]
public class Area : MonoBehaviour
{
    [ListDrawerSettings(HideAddButton = true, HideRemoveButton = true, Expanded = true, DraggableItems = false)]
    public List<PathWay> pathWays;
    public BoxCollider boundsCol;
    public Transform rootObj;
    public Transform mapImage;
    public bool autoMapSprite = true;
    public NavMeshSurface surface;
    public GameObject[] hideOnExit;
    public bool startRoom;
    public bool isDoor;
    AreaData spawnInfo;

    public AreaData SpawnInfo { get => spawnInfo; set => spawnInfo = value; }

    protected virtual void Awake()
    {
        if (autoMapSprite)
            mapImage.transform.localScale = new Vector3(boundsCol.size.x, boundsCol.size.z, 1) * 23.5f;
        AreaTriggers trigger = GetComponentInChildren<AreaTriggers>();
        if (isDoor)
        {
            Destroy(trigger);
        }
        else
        {
            if (trigger == null)
                trigger = gameObject.AddComponent<AreaTriggers>();
            trigger.Init(this);
        }
        Publisher.Subscribe<GameStartEvent>(OnGameStart);
    }
    public void SetMapColor(Color c)
    {
        mapImage.GetComponentInChildren<SpriteRenderer>().color = c;
    }
    private void OnDestroy()
    {
        Publisher.Unsubscribe<GameStartEvent>(OnGameStart);
    }
    public void OnGameStart(GameStartEvent e)
    {
        if (surface && surface.enabled)
            surface.BuildNavMesh();
        if (!startRoom)
            Hide();
        if (isDoor)
            transform.parent.SetParent(LevelManager.Instance.transform);
        else
        {
            List<PathWay> openPaths = GetOpenPaths();
            foreach (PathWay path in openPaths)
            {
                Area deadEndInstance = e.levelGen.SpawnDoor(path, e.level.deadEndHoriz.prefab, GlobalData.Instance.lockedDoor);
                deadEndInstance.OnGameStart(e);
            }
        }
    }
    public void Hide()
    {
        if (isDoor) return;
        foreach (Transform t in transform.parent)
        {
            bool show = (t == transform.parent || t == transform);
            t.gameObject.SetActive(show);
        }
    }
    public void Show()
    {
        if (isDoor) return;
        foreach (Transform t in transform.parent)
        {
            bool show = true;
            t.gameObject.SetActive(show);
        }
    }
    [Button("Validate")]
    public void OnValidate()
    {
        boundsCol = GetComponent<BoxCollider>();
        rootObj = transform.root;
        pathWays = new List<PathWay>(rootObj.GetComponentsInChildren<PathWay>());
    }
#if UNITY_EDITOR
    [Button]
    public void CreateAreaData()
    {
        OnValidate();
        AreaData areaData = ScriptableObject.CreateInstance<AreaData>();
        string assetName = transform.root.gameObject.name;
        areaData.prefab = transform.root.gameObject;
        UnityEditor.AssetDatabase.CreateAsset(areaData, "Assets/LevelData/" + assetName + ".asset");
        UnityEditor.AssetDatabase.SaveAssets();
    }
#endif
    public void DestoryRoom()
    {
        foreach (PathWay path in pathWays)
        {
            if (path.HasConnectedRoom)
            {
                path.Unlink();
            }
        }
        Destroy(rootObj.gameObject);
    }

    public PathWay GetRandomOpenPath()
    {
        List<PathWay> freeRooms = GetOpenPaths();
        if (freeRooms.Count == 0)
            return null;

        return freeRooms[UnityEngine.Random.Range(0, freeRooms.Count)];
    }
    public int GetRandomOpenPathIndex()
    {
        PathWay result = GetRandomOpenPath();
        if (result == null)
            return -1;
        return pathWays.IndexOf(result);
    }
    public List<PathWay> GetOpenPaths()
    {
        return pathWays.FindAll(IsFree);
    }
    public List<int> GetOpenPathIndices()
    {
        List<int> result = new List<int>();
        for (int i = 0; i < pathWays.Count; i++)
        {
            if (IsFree(pathWays[i]))
                result.Add(i);
        }
        return result;
    }
    private bool IsFree(PathWay obj)
    {
        return !obj.HasConnectedRoom;
    }
}

public class AreaEnterEvent : PublisherEvent
{
    public Area area;
    public AreaData areaData;

    public AreaEnterEvent(Area area, AreaData data)
    {
        this.area = area;
        this.areaData = data;
    }
}
public class GameStartEvent : PublisherEvent
{
    public LevelGenerator levelGen;
    public LevelData level;

    public GameStartEvent(LevelGenerator levelGen, LevelData level)
    {
        this.levelGen = levelGen;
        this.level = level;
    }
}