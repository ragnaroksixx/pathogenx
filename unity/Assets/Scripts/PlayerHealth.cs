﻿using UnityEngine;
using System.Collections;
using System;

public class PlayerHealth : HealthScript
{
    bool isDead;
    public CustomAudioClip damageSFX, deathSFX;
    public ThirdPersonController player;

    public override void Awake()
    {
        base.Awake();
        OnHealthChanged();
    }
    private void OnDestroy()
    {

    }
    public void SetIFrames(float time)
    {
        iFramesTrack = Mathf.Max(iFramesTrack, time + Time.time);
    }
    float iFrames = 1f;
    float iFramesTrack;
    public override void TakeDamage(int amount)
    {
        if (isDead) return;
        if (Time.time < iFramesTrack) return;
        WinMenu.AddStat("damageTaken", amount);
        SetIFrames(iFrames);
        damageSFX.Play();
        base.TakeDamage(amount);
        OnHealthChanged();
    }
    public override void Heal(int amount)
    {
        base.Heal(amount);
        OnHealthChanged();
    }
    protected override void Die()
    {
        if (isDead) return;
        deathSFX.Play();
        LevelManager.GameOver();
        isDead = true;
    }

    void OnHealthChanged()
    {
        player.Life = currentHP;
    }

}

