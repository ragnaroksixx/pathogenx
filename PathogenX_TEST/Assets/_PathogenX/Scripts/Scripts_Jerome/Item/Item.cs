﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Item : MonoBehaviour
{
    public string displayName;
    [TextArea(5, 20)]
    public string description;
    public Sprite image;
    public int qty;
    public bool isKey = false;
    public virtual void Discard()
    {
        qty = 0;
        Publisher.Raise(new UpdateItemUIEvent());
    }
    public virtual int GetCount()
    {
        return qty;
    }
    public virtual void SetUI(ItemUI ui)
    {
        int q = GetCount();
        if (q > 1)
        {
            ui.qtyText.text = q.ToString();
            ui.qtyRoot.SetActive(true);
        }
        else
            ui.qtyRoot.SetActive(false);
        ui.equipRoot.SetActive(false);
        ui.itemImage.sprite = image;
        ui.itemImage.gameObject.SetActive(true);
    }

    public virtual List<ItemAction> GetActions()
    {
        List<ItemAction> actions = new List<ItemAction>();
        if (!isKey)
            actions.Add(new ItemAction("DISCARD", DiscardPrompt));
        return actions;
    }

    public void DiscardPrompt()
    {
        Publisher.Raise(new ConfirmationEvent("Are you sure you want to discard this item?"
            , () => { Discard(); }));
    }
}
