﻿using UnityEngine;
using System.Collections;

public class StartPointAreaTrigger : AreaTriggers
{
    public Transform bossSpawn;
    public GameObject endInteractable;
    public override void OnAreaEnter()
    {
        if (area == null) return;
        base.OnAreaEnter();
        if (Inventory.Instance.HasItem("Vaccine"))
        {
            Publisher.Raise(new BossFightEvent(bossSpawn));
            Publisher.Subscribe<BossDeathtEvent>(BossDefeat);
        }
        else
        {
            endInteractable.gameObject.SetActive(false);
        }
    }
    protected override void OnDestroy()
    {
        base.OnDestroy();
        Publisher.Unsubscribe<BossDeathtEvent>(BossDefeat);
    }
    public void BossDefeat(BossDeathtEvent e)
    {
        endInteractable.gameObject.SetActive(true);
    }
}
