﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System;

public static class SceneController
{
    static string initScene = "error";
    static AsyncOperation Load(string scene)
    {
        return SceneManager.LoadSceneAsync(scene, LoadSceneMode.Additive);
    }
    static void UnloadScene(string scene)
    {
        if (!SceneManager.GetSceneByName(scene).isLoaded)
            return;
        SceneManager.UnloadSceneAsync(scene);
    }

    public static void LoadAreaTransition(string scene = "AreaTransitionLoading")
    {
        Load(scene);
    }
    public static void UnloadAreaTransition(string scene = "AreaTransitionLoading")
    {
        UnloadScene(scene);
    }
    public static void SetInitScene()
    {
        Scene activeScene = SceneManager.GetActiveScene();
        string activeScenePath = GetActiveScenePath(activeScene);
        initScene = activeScenePath;
    }
    public static void LoadInitScene()
    {
        SceneManager.LoadScene(initScene);
    }
    public static void ReloadLevelScene()
    {
        AsyncOperation ao = SceneManager.UnloadSceneAsync("LevelGeneration", UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);
        if (ao == null)
        {
            LoadScenesAdditive("LevelGeneration");
        }
        else
        {
            ao.completed += (AsyncOperation a) => { LoadScenesAdditive("LevelGeneration"); };
        }
    }
    private static string GetActiveScenePath(Scene activeScene)
    {
        string activeScenePath = activeScene.path;
        activeScenePath = activeScenePath.Replace(".unity", "");
        activeScenePath = activeScenePath.Replace("Assets/", "");
        return activeScenePath;
    }

    public static void LoadScenesAdditive(params string[] sceneList)
    {
        Scene activeScene = SceneManager.GetActiveScene();
        string activeScenePath = GetActiveScenePath(activeScene);

        for (int i = 0; i < sceneList.Length; i++)
        {
            string scene = sceneList[i];
            if (!IsScene_CurrentlyLoaded_inEditor(scene))
            {
                SceneManager.LoadSceneAsync(scene, LoadSceneMode.Additive);
            }
        }
    }

    static bool IsScene_CurrentlyLoaded_inEditor(string sceneName_no_extention)
    {
        for (int i = 0; i < SceneManager.sceneCount; ++i)
        {
            var scene = SceneManager.GetSceneAt(i);

            if (scene.name == sceneName_no_extention)
            {
                return true;//the scene is already loaded
            }
        }
        //scene not currently loaded in the hierarchy:
        return false;
    }
}
