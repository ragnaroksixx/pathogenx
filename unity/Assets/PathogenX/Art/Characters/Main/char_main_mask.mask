%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: char_main_mask
  m_Mask: 01000000010000000100000001000000010000000000000000000000000000000000000001000000010000000000000000000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: char_main
    m_Weight: 1
  - m_Path: helper_body
    m_Weight: 1
  - m_Path: helper_body/helper_waist
    m_Weight: 1
  - m_Path: helper_body/helper_waist/b_hips
    m_Weight: 1
  - m_Path: helper_body/helper_waist/b_hips/b_spine
    m_Weight: 1
  - m_Path: helper_body/helper_waist/b_hips/b_spine/b_chest
    m_Weight: 1
  - m_Path: helper_body/helper_waist/b_hips/b_spine/b_chest/b_chest_upper
    m_Weight: 1
  - m_Path: helper_body/helper_waist/b_hips/b_spine/b_chest/b_chest_upper/b_neck
    m_Weight: 1
  - m_Path: helper_body/helper_waist/b_hips/b_spine/b_chest/b_chest_upper/b_neck/b_head
    m_Weight: 1
  - m_Path: helper_body/helper_waist/b_hips/b_spine/b_chest/b_chest_upper/b_neck/b_head/b_head_end
    m_Weight: 1
  - m_Path: helper_body/helper_waist/b_hips/b_spine/b_chest/b_chest_upper/b_shoulder_left
    m_Weight: 1
  - m_Path: helper_body/helper_waist/b_hips/b_spine/b_chest/b_chest_upper/b_shoulder_left/b_bicep_left
    m_Weight: 1
  - m_Path: helper_body/helper_waist/b_hips/b_spine/b_chest/b_chest_upper/b_shoulder_left/b_bicep_left/b_forearm_left
    m_Weight: 1
  - m_Path: helper_body/helper_waist/b_hips/b_spine/b_chest/b_chest_upper/b_shoulder_left/b_bicep_left/b_forearm_left/b_hand_left
    m_Weight: 1
  - m_Path: helper_body/helper_waist/b_hips/b_spine/b_chest/b_chest_upper/b_shoulder_left/b_bicep_left/b_forearm_left/b_hand_left/b_hand_left_end
    m_Weight: 1
  - m_Path: helper_body/helper_waist/b_hips/b_spine/b_chest/b_chest_upper/b_shoulder_right
    m_Weight: 1
  - m_Path: helper_body/helper_waist/b_hips/b_spine/b_chest/b_chest_upper/b_shoulder_right/b_bicep_right
    m_Weight: 1
  - m_Path: helper_body/helper_waist/b_hips/b_spine/b_chest/b_chest_upper/b_shoulder_right/b_bicep_right/b_forearm_right
    m_Weight: 1
  - m_Path: helper_body/helper_waist/b_hips/b_spine/b_chest/b_chest_upper/b_shoulder_right/b_bicep_right/b_forearm_right/b_hand_right
    m_Weight: 1
  - m_Path: helper_body/helper_waist/b_hips/b_spine/b_chest/b_chest_upper/b_shoulder_right/b_bicep_right/b_forearm_right/b_hand_right/b_hand_right_end
    m_Weight: 1
  - m_Path: helper_body/helper_waist/b_hips/b_thigh_left
    m_Weight: 1
  - m_Path: helper_body/helper_waist/b_hips/b_thigh_left/b_shin_left
    m_Weight: 1
  - m_Path: helper_body/helper_waist/b_hips/b_thigh_left/b_shin_left/b_foot_left
    m_Weight: 1
  - m_Path: helper_body/helper_waist/b_hips/b_thigh_left/b_shin_left/b_foot_left/b_foot_left_end
    m_Weight: 1
  - m_Path: helper_body/helper_waist/b_hips/b_thigh_right
    m_Weight: 1
  - m_Path: helper_body/helper_waist/b_hips/b_thigh_right/b_shin_right
    m_Weight: 1
  - m_Path: helper_body/helper_waist/b_hips/b_thigh_right/b_shin_right/b_foot_right
    m_Weight: 1
  - m_Path: helper_body/helper_waist/b_hips/b_thigh_right/b_shin_right/b_foot_right/b_foot_right_end
    m_Weight: 1
  - m_Path: helper_body/helper_waist/IK_hand_left
    m_Weight: 1
  - m_Path: helper_body/helper_waist/IK_hand_right
    m_Weight: 1
  - m_Path: helper_body/IK_foot_left
    m_Weight: 1
  - m_Path: helper_body/IK_foot_right
    m_Weight: 1
