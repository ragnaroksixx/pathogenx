﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Gun : Item
{
    public Weapon.WeaponType weaponType;
    public override int GetCount()
    {
        qty = 0;
        for (int i = 0; i < Inventory.Instance.player.Weapons.Length; i++)
        {
            if (Inventory.Instance.player.Weapons[i].Type == weaponType)
            {
                if (Inventory.Instance.player.Weapons[i].Unlocked)
                {
                    qty += Inventory.Instance.player.Weapons[i].BulletsAmounts;
                    if (qty == 0)
                        qty = 1;
                }
                break;
            }
        }
        return base.GetCount();
    }
    public override List<ItemAction> GetActions()
    {
        List<ItemAction> actions = base.GetActions();
        actions.Insert(0, new ItemAction("Reload", Reload));
        for (int i = 0; i < Inventory.Instance.player.Weapons.Length; i++)
        {
            if (Inventory.Instance.player.Weapons[i].Type == weaponType)
            {
                if (Inventory.Instance.player.WeaponInUse == Inventory.Instance.player.Weapons[i])
                    actions.Insert(0, new ItemAction("Un-Equip", Dequip));
                else
                    actions.Insert(0, new ItemAction("Equip", Equip));
                break;
            }
        }
        return actions;
    }
    public void Equip()
    {
        Debug.LogWarning("Equip");
        for (int i = 0; i < Inventory.Instance.player.Weapons.Length; i++)
        {
            if (Inventory.Instance.player.Weapons[i].Type == weaponType)
            {
                Inventory.Instance.player.Equip(i);
                break;
            }
        }
        //TimeHandler.Instance.UnPause();
        Publisher.Raise(new UpdateItemUIEvent());
    }
    public void Dequip()
    {
        Debug.LogWarning("Dequip");
        for (int i = 0; i < Inventory.Instance.player.Weapons.Length; i++)
        {
            if (Inventory.Instance.player.Weapons[i].Type == weaponType)
            {
                Inventory.Instance.player.Equip(-1);
                break;
            }
        }
        //TimeHandler.Instance.UnPause();
        Publisher.Raise(new UpdateItemUIEvent());
    }
    public void Reload()
    {
        Debug.LogWarning("Reload");
        for (int i = 0; i < Inventory.Instance.player.Weapons.Length; i++)
        {
            if (Inventory.Instance.player.Weapons[i].Type == weaponType)
            {
                Inventory.Instance.player.Reload(Inventory.Instance.player.Weapons[i]);
                break;
            }
        }
        Publisher.Raise(new UpdateItemUIEvent());
    }
    public override void SetUI(ItemUI ui)
    {
        base.SetUI(ui);
        for (int i = 0; i < Inventory.Instance.player.Weapons.Length; i++)
        {
            if (Inventory.Instance.player.Weapons[i].Type == weaponType)
            {
                if (Inventory.Instance.player.WeaponInUse == Inventory.Instance.player.Weapons[i])
                    ui.equipRoot.SetActive(true);
                else
                    ui.equipRoot.SetActive(false);
                break;
            }
        }
    }
    public override void Discard()
    {
        for (int i = 0; i < Inventory.Instance.player.Weapons.Length; i++)
        {
            if (Inventory.Instance.player.Weapons[i].Type == weaponType)
            {
                if (Inventory.Instance.player.Weapons[i] == Inventory.Instance.player.WeaponInUse)
                    Inventory.Instance.player.Equip(-1);
                Inventory.Instance.player.Weapons[i].Unlocked = false;
                Inventory.Instance.player.Weapons[i].BulletsAmounts = 0;
                qty = 0;
                break;
            }
        }
        base.Discard();
    }
}
