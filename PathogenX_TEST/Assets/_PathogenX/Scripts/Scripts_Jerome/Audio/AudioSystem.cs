﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AudioSystem : MonoBehaviour
{
    AudioPool audioPool;
    public static AudioSystem Instance;
    AudioObject bgm = null;
    float bgmVolumePercentage = .75f;
    float sfxVolumePercentage = 1;
    float masterVolumePercentage = .25f;

    public float BGMvolumePercentage { get => bgmVolumePercentage; }
    public float SFXvolumePercentage { get => sfxVolumePercentage; }
    public float MASTERvolumePercentage { get => masterVolumePercentage; }
    AudioPool AudioPool { get => audioPool; }
    public AudioObject BGM { get => bgm; }

    public CustomAudioClip EmptyBGM, EmptySFX;
    public GameObject AudioSourcePrefabGameObject;
    private void Awake()
    {
        if (Instance)
        {
            Destroy(this.gameObject);
        }
        else
        {
            DontDestroyOnLoad(this.gameObject);
            AudioDictionary.Init();
            Instance = this;
            audioPool = new AudioPool(AudioSourcePrefabGameObject, 10);
        }
    }

    public static AudioObject PlayBGM(CustomAudioClip clip, Vector3 location, float fadeInTime = -1)
    {
        if (clip == null)
            clip = Instance.EmptyBGM;
        return Play(clip, location, fadeInTime);
    }
    public static AudioObject PlaySFX(CustomAudioClip clip, Vector3 location, float fadeInTime = -1)
    {
        if (clip == null)
            clip = Instance.EmptySFX;
        return Play(clip, location, fadeInTime);
    }
    static AudioObject Play(CustomAudioClip clip, Vector3 location, float fadeInTime = -1)
    {
        AudioObject obj = Instance.audioPool.Pop();
        obj.Source.ignoreListenerPause = clip.playWhilePaused;
        obj.Source.transform.position = location;
        obj.Source.clip = clip.Clip;

        obj.clip = clip;


        obj.Source.loop = clip.loop;

        if (fadeInTime == -1)
        {
            obj.Source.volume = GetVolume(clip);
            obj.Source.time = clip.startTime;
            obj.Source.Play();
        }
        else
        {
            obj.Source.volume = 0;
            obj.FadeIn(fadeInTime);
        }

        if (clip.category == AudioCategory.BGM)
        {
            if (Instance.bgm != null)
                FadeOutAndStop(Instance.bgm, fadeInTime);
            Instance.bgm = obj;
        }
        return obj;
    }

    public static void StopInstant(AudioObject obj)
    {
        Instance.audioPool.Push(obj);
    }
    public static void FadeOutAndStop(AudioObject obj, float time = -1)
    {
        if (obj == Instance.bgm)
            Instance.bgm = null;
        obj.FadeOut(time);
    }
    public static void SetBGMPercentage(int percent)
    {
        Instance.bgmVolumePercentage = (float)percent / 100.0f;
        Instance.audioPool.RefreshAll();
    }
    public static void SetSFXPercentage(int percent)
    {
        Instance.sfxVolumePercentage = (float)percent / 100.0f;
        Instance.audioPool.RefreshAll();
    }
    public static void SetMasterPercentage(int percent)
    {
        Instance.masterVolumePercentage = (float)percent / 100.0f;
        Instance.audioPool.RefreshAll();
    }
    public static float GetVolume(CustomAudioClip clip)
    {
        float volumePercentage = clip.category == AudioCategory.BGM ? Instance.bgmVolumePercentage : Instance.sfxVolumePercentage;
        return clip.Volume * volumePercentage * Instance.masterVolumePercentage;
    }
    public static void StopAll()
    {
        Instance.audioPool.StopAll();
    }
}
public static class AudioDictionary
{
    static Dictionary<string, CustomAudioClip> dict;
    static string AUDIO_CLIP_PATH = "AudioObjects";
    static AudioDictionary()
    {
        CustomAudioClip[] allAudio = Resources.LoadAll<CustomAudioClip>(AUDIO_CLIP_PATH);
        dict = new Dictionary<string, CustomAudioClip>();
        foreach (CustomAudioClip item in allAudio)
        {
            if (!dict.ContainsKey(item.name))
            {
                dict.Add(item.name.ToLower(), item);
                item.Init();
            }
            else
            {
                Debug.LogError("Duplicate Audio Clip: " + item.name);
            }
        }
    }
    public static CustomAudioClip GetClip(string name)
    {
        name = name.ToLower();
        if (dict.ContainsKey(name))
            return dict[name];

        Debug.LogError("Audio Clip Not Found");
        return null;
    }
    public static void Init()
    {

    }
}
public enum AudioCategory
{
    SFX,
    BGM,
}
