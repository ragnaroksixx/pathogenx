﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using DG.Tweening;

public class HUD : MonoBehaviour
{
    public CanvasGroup damageVingette;

    private void Start()
    {
        Publisher.Subscribe<PlayerHealthEvent>(OnHealthChanged);
    }
    private void OnDestroy()
    {
        Publisher.Unsubscribe<PlayerHealthEvent>(OnHealthChanged);
    }

    public void OnHealthChanged(PlayerHealthEvent e)
    {
        damageVingette.DOFade(1 - e.normalizedHP, 0.1f);
    }
}

