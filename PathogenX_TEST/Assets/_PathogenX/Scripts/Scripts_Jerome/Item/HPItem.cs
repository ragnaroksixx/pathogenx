﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HPItem : Item
{
    public int healAmount;
    public override List<ItemAction> GetActions()
    {
        List<ItemAction> actions = base.GetActions();
        actions.Insert(0, new ItemAction("Use", Use));
        return actions;
    }
    public void Use()
    {
        if (Player.Instance.health.currentHP == Player.Instance.health.maxHP) return;
        Debug.LogWarning("Recover " + healAmount);
        Player.Instance.health.Heal(healAmount);
        qty--;
        Publisher.Raise(new UpdateItemUIEvent());
    }
}
