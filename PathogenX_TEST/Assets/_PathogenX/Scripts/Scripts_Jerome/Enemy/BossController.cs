﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossController : MonoBehaviour
{
    public bool isActive;
    int stunTime = 3;
    int stunTrack = 0;
    public BossAI boss;
    public bool isFinalFight;
    // Start is called before the first frame update
    void Start()
    {
        boss.gameObject.SetActive(false);
        isActive = false;
        Publisher.Subscribe<AreaEnterEvent>(OnAreaEnter);
        Publisher.Subscribe<ItemPickUpEvent>(OnItemPickUp);
        Publisher.Subscribe<BossFightEvent>(SpawnFinalFight);
    }
    private void OnDestroy()
    {
        Publisher.Unsubscribe<AreaEnterEvent>(OnAreaEnter);
        Publisher.Unsubscribe<ItemPickUpEvent>(OnItemPickUp);
        Publisher.Unsubscribe<BossFightEvent>(SpawnFinalFight);
    }
    // Update is called once per frame
    void Update()
    {

    }
    public void StunBoss()
    {
        stunTrack = stunTime;
    }
    public void UnStun()
    {
        boss.Health.Heal(boss.Health.maxHP);
        boss.anim.SetTrigger("forceidle");
        boss.SetState(BasicAI.AIState.IDLE);
    }
    public void OnItemPickUp(ItemPickUpEvent e)
    {
        if (e.item.ToLower() == "vaccine")
        {
            isActive = true;
        }
    }
    public void OnAreaEnter(AreaEnterEvent e)
    {
        if (!isActive) return;
        if (stunTrack > 0)
        {
            stunTrack--;
            if (stunTrack > 0)
                return;
            else
                UnStun();
        }
        if (boss.IsDowned) return;
        if (e.areaData.ignoreBossSpawn) return;

        boss.gameObject.SetActive(false);
        List<PathWay> paths = new List<PathWay>(e.area.pathWays);
        PathWay path = null;
        float closestVal = Mathf.Infinity;
        Debug.LogError(e.area.rootObj.name);
        foreach (PathWay p in paths)
        {
            float dist = Vector3.Distance(p.transform.position, Player.Instance.transform.position);
            if (closestVal > dist)
            {
                path = p;
                closestVal = dist;
            }
        }
        paths.Remove(path);
        if (paths.Count == 0)
            return;
        path = paths[Random.Range(0, paths.Count)];
        boss.transform.position = path.transform.position + (path.transform.forward * -1);
        boss.gameObject.SetActive(true);
    }
    public void SpawnFinalFight(BossFightEvent e)
    {
        isFinalFight = true;
        StopAllCoroutines();
        UnStun();
        boss.gameObject.SetActive(false);
        boss.transform.position = e.spawnPoint.position;
        boss.transform.rotation = e.spawnPoint.rotation;
        boss.Health.maxHP = Mathf.FloorToInt(boss.Health.maxHP * 1.5f);
        boss.Health.Heal(boss.Health.maxHP);
        boss.SetState(BasicAI.AIState.IDLE);
        boss.gameObject.SetActive(true);
    }
}
public class BossFightEvent : PublisherEvent
{
    public Transform spawnPoint;

    public BossFightEvent(Transform spawnPoint)
    {
        this.spawnPoint = spawnPoint;
    }
}
public class BossDeathtEvent : PublisherEvent
{

}
