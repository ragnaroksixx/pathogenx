﻿using UnityEngine;
using System.Collections;
using UnityEngine.AI;

public class BossAI : MeleeAI
{
    public BossController controller;
    public Hurtbox hurtbox;
    HealthScript health;
    public bool IsDowned { get => aiState == AIState.DOWNED; }
    public HealthScript Health { get => health; set => health = value; }

    public override void Awake()
    {
        base.Awake();
        health = hurtbox.healthRef;

    }
    public override void OnDie()
    {
        if (controller.isFinalFight)
        {
            base.OnDie();
            Publisher.Raise(new BossDeathtEvent());
        }
        else
        {
            controller.StunBoss();
            hb.gameObject.SetActive(false);
            WinMenu.AddStat("score", GetComponentInChildren<HealthScript>().deathScore);
            SetState(AIState.DOWNED);
        }
    }
    public override void SetState(AIState state)
    {
        base.SetState(state);
        hurtbox.enabled = !IsDowned;
        col.enabled = !IsDowned && state != AIState.DEAD;
    }
}

