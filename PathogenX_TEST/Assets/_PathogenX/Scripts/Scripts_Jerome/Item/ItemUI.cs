﻿using UnityEngine;
using System.Collections;
using TMPro;
using System;
using UnityEngine.UI;
using System.Collections.Generic;

public class ItemUI : MonoBehaviour
{
    int index;
    public GameObject qtyRoot, equipRoot;
    public TMP_Text qtyText;
    public Image itemImage, highlightImage;
    Item i;
    private void Awake()
    {
        index = transform.GetSiblingIndex();
        UpdateUI();
        Publisher.Subscribe<PauseEvent>(OnPasueEvent);
        Publisher.Subscribe<SelectItemEvent>(OnSelectItem);
        Publisher.Subscribe<UpdateItemUIEvent>(OnUpdate);
        DeselecteItem();
    }
    private void OnDestroy()
    {
        Publisher.Unsubscribe<PauseEvent>(OnPasueEvent);
        Publisher.Unsubscribe<SelectItemEvent>(OnSelectItem);
        Publisher.Unsubscribe<UpdateItemUIEvent>(OnUpdate);
    }

    private void OnSelectItem(SelectItemEvent e)
    {
        if (i != e.item || i == null)
            DeselecteItem();
    }

    public void OnPasueEvent(PauseEvent e)
    {
        if (e.isPaused)
            UpdateUI();
    }
    public void OnUpdate(UpdateItemUIEvent e)
    {
        UpdateUI();
        if (selected)
            SelectItem();
    }
    public void UpdateUI()
    {
        ClearUI();
        if (Inventory.Instance == null)
            return;

        List<Item> items = Inventory.Instance.GetInventory();
        if (items.Count <= index)
            return;
        i = Inventory.Instance.GetInventory()[index];
        i.SetUI(this);
    }

    private void ClearUI()
    {
        i = null;
        qtyRoot.SetActive(false);
        equipRoot.SetActive(false);
        itemImage.gameObject.SetActive(false);
    }
    bool selected;
    public void SelectItem()
    {
        selected = true;
        highlightImage.gameObject.SetActive(true);
        Publisher.Raise(new SelectItemEvent(i));
    }
    public void DeselecteItem()
    {
        selected = false;
        highlightImage.gameObject.SetActive(false);
    }
}

public class UpdateItemUIEvent : PublisherEvent
{

}
