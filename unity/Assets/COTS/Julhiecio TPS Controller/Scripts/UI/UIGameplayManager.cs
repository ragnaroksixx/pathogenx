﻿using UnityEngine;
using UnityEngine.UI;

[AddComponentMenu("Julhiecio TPS Controller /UI/UI Gameplay")]
public class UIGameplayManager : MonoBehaviour
{
	
	ThirdPersonController m_player;

	[Header("UI Settings")]
	public bool IsMobile;
	public bool HiddenAllUiWhenDie;
	public bool HiddenUIWhenPressF2;
	public GameObject All_UI;
	public GameObject MobileUIPanel;
	public GameObject MobileButtonsPanel;
	public GameObject MobileButtonsDrivingPanel;
	public bool ShowInteractText;
	public Text InteractText;
	[HideInInspector] private Touchfield MobileTouchfield;
	[HideInInspector] private bool _controllmobilepanels;

	[Header("Crosshair Information")]
	public bool ScaleCrosshairWithPrecision;
	public bool HiddenWhenDriving;
	public Image Crosshair;
	[Range(0.01f, 0.2f)]
	public float CrosshairSensibility = 0.15f;
	Vector3 CrosshairStartSize;
	float CurrentSize;

	[Header("Weapon Information")]
	public bool DisplayWeaponInformation;
	public Text WeaponName;
	public Text BulletsCount;

	[Header("Player Stats")]
	public bool DisplayHeatlhBar;
	public Image HealthBar;



	void Start()
	{
		CrosshairStartSize = Crosshair.transform.localScale;
		CurrentSize = CrosshairStartSize.x;
		if (m_player == null)
		{
			m_player = FindObjectOfType<ThirdPersonController>();
		}
		Invoke("enable_mobile_panels_controll", 0.1f);
		MobileTouchfield = GameObject.Find("Touchfield").GetComponent<Touchfield>();
		MobileTouchfield.gameObject.SetActive(IsMobile);
	}

	void Update()
	{
		ControllCrosshair();
		DisplayWeaponInfo();
		DisplayHealth();
		MobileUI();
		if (m_player.IsDead == true && HiddenAllUiWhenDie == true && this.gameObject.activeInHierarchy == true)
		{
			this.gameObject.SetActive(false);
		}

        if (Input.GetKeyDown(KeyCode.F2) && HiddenUIWhenPressF2 == true)
        {
			All_UI.SetActive(!All_UI.activeInHierarchy);
        }
		if (IsMobile == false)
		{
			MobileUIPanel.SetActive(false);
		}

		if (ShowInteractText == true)
		{
			if (m_player.ToPickupWeapon == true)
			{
				InteractText.text = "Press [F] to pick up weapon";
			}
			if (m_player.ToEnterVehicle == true)
			{
				InteractText.text = "Press [F] to drive the vehicle";
			}
			if (m_player.ToEnterVehicle == false && m_player.ToPickupWeapon == false )
			{
				InteractText.text = "";
			}
		}

	}
	public void MobileUI()
    {
		if (_controllmobilepanels == true)
		{
			MobileButtonsPanel.SetActive(!m_player.IsDriving);
			MobileButtonsDrivingPanel.SetActive(m_player.IsDriving);
		}
    }
	public void ControllCrosshair()
    {
		if (ScaleCrosshairWithPrecision == false)
			return;
		//Crosshair Size
		CurrentSize = Mathf.Lerp(CurrentSize, CrosshairStartSize.x, 5 * Time.deltaTime);
		Vector3 crosshairsize = new Vector3(CurrentSize, CurrentSize, CurrentSize);
		Crosshair.transform.localScale = crosshairsize;

		//Shot increase the crosshair scale
		if (m_player.IsArmed)
		{
			CurrentSize = CurrentSize + 3*m_player.ShotErrorProbability * CrosshairSensibility;
		}
		//Hidden crosshair when driving
		if (HiddenWhenDriving)
		{
			Crosshair.gameObject.SetActive(!m_player.IsDriving);
		}
	}
	public void DisplayWeaponInfo()
    {
		if (DisplayWeaponInformation == false)
			return;
		if (m_player.WeaponInUse != null && DisplayWeaponInformation)
		{
			//Bullets counts
			if (m_player.WeaponInUse.TotalBullets > 0 || m_player.WeaponInUse.BulletsAmounts > 0)
			{
				BulletsCount.text = m_player.WeaponInUse.BulletsAmounts + "/" + m_player.WeaponInUse.TotalBullets;
				BulletsCount.color = Color.white;
			}
			if (m_player.WeaponInUse.TotalBullets <= 0 && m_player.WeaponInUse.BulletsAmounts <= 0)
			{
				BulletsCount.text = "No ammo";
				BulletsCount.color = Color.red;
			}
			//Weapon name
			WeaponName.text = m_player.WeaponInUse.WeaponName;
		}
		else
		{
			//Bullets counts
			BulletsCount.text = "";

			//Weapon name
			WeaponName.text = "";
		}
	}
	public void DisplayHealth()
    {
		if (DisplayHeatlhBar == false)
			return;
		//LifeBar
		HealthBar.fillAmount = Mathf.Lerp(HealthBar.fillAmount, m_player.Life / 100, 10 * Time.deltaTime);
	}

	private void enable_mobile_panels_controll()
    {
		_controllmobilepanels = true;
    }
}
