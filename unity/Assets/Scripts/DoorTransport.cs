﻿using UnityEngine;
using System.Collections;
using System;

public class DoorTransport : SimpleKeyInteractable
{
    public Transform toPoint;
    public bool isLocked;
    private void Awake()
    {
        Publisher.Subscribe<BossFightEvent>(SpawnFinalFight);
    }

    private void SpawnFinalFight(BossFightEvent e)
    {
        isLocked = true;
    }

    private void OnDestroy()
    {
        Publisher.Unsubscribe<BossFightEvent>(SpawnFinalFight);
    }
    public override void OnPress()
    {
        if (isLocked) return;
        if (Time.time <= DisableDoorEvent.unlockTime) return;
        inTrigger = false;
        EnemySpawnHelper.numRoomsTraversed++;
        Publisher.Raise(new HideNotificationEvent());
        StartCoroutine(OnUse(player.transform));
    }
    public static bool hackLockCameraInDoor;
    IEnumerator OnUse(Transform player)
    {
        isActive = false;
        TimeHandler.Instance.inputLock = true;
        hackLockCameraInDoor = true;
        Vector3 pos = toPoint.transform.position;
        pos.y = player.position.y;
        player.gameObject.SetActive(false);
        float time = 5f;
        DisableDoorEvent.DelayDoor(1);
        SceneTransitionEffect.waitTime = time;
        SceneController.LoadAreaTransition();
        yield return null;
        player.position = pos;
        player.gameObject.SetActive(true);
        DisableDoorEvent.DelayDoor(1);
        yield return new WaitForSecondsRealtime(time);
        DisableDoorEvent.DelayDoor(2);
        TimeHandler.Instance.inputLock = false;
        hackLockCameraInDoor = false;
        int id = Inventory.Instance.player.WeaponInUse
            ? Inventory.Instance.player.WeaponInUse.WeaponID : -1;
        Inventory.Instance.player.Equip(id);
        isActive = true;
    }
    public virtual void Unlock()
    {
        isLocked = false;
    }
}

public class DisableDoorEvent
{
    public static float unlockTime = 0;

    public static void DelayDoor(float seconds = 1)
    {
        unlockTime = Mathf.Max(unlockTime, Time.time + seconds);
    }
}
