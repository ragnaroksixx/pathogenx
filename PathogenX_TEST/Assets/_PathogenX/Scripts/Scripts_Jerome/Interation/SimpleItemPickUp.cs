﻿using UnityEngine;
using System.Collections;

public class SimpleItemPickUp : SimpleKeyInteractable
{
    public string item;
    public int count = 1;
    public bool isObjectiveItem;
    public RPGTalkArea RPGTalkArea;
    public override void OnPress()
    {
        if (Inventory.Instance.IsFull() && !Inventory.Instance.HasItem(item))
        {
            Publisher.Raise(new NotificationEvent("<color=red>Inventory Full"));
            return;
        }
        RPGTalkArea?.StartTalk();
        if (isObjectiveItem)
            Publisher.Raise(new RefreshSpawnsEvent());
        onEnter?.Play();
        Inventory.Instance.Add(item, 1);
        Publisher.Raise(new ItemPickUpEvent(item));
        Publisher.Raise(new HideNotificationEvent());
        Destroy(transform.GetChild(0).gameObject);
        Destroy(GetComponent<Animator>());
        GetComponentInChildren<Collider>().enabled = false;
        enabled = false;
    }
    public override string GetText()
    {
        return base.GetText() + " [" + keyPress.ToString() + "]"; ;
    }
}

public class ItemPickUpEvent : PublisherEvent
{
    public string item;

    public ItemPickUpEvent(string item)
    {
        this.item = item;
    }
}
