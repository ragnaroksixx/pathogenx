﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using System.Linq;
using TMPro;

public class SettingsMenu : MonoBehaviour
{
    public AudioMixer audioMixer;

    public TMP_Dropdown resolutionDropdown;
    public TMP_Dropdown qualityDropdown;
    public Toggle fullScreenToggle;
    public Slider volumeSlider;
    public Slider sensitivitySlider;
    Resolution[] resolutions;

    void Awake()
    {
        resolutions = Screen.resolutions.Select(resolution => new Resolution { width = resolution.width, height = resolution.height }).Distinct().ToArray();

        resolutionDropdown.ClearOptions();

        List<string> options = new List<string>();

        int currentResolutionIndex = 0;
        for (int i = 0; i < resolutions.Length; i++)
        {
            string option = resolutions[i].width + " x " + resolutions[i].height;
            options.Add(option);

            if (resolutions[i].width == Screen.currentResolution.width &&
                resolutions[i].height == Screen.currentResolution.height)
            {
                currentResolutionIndex = i;
            }
        }

        resolutionDropdown.AddOptions(options);
        if (PlayerPrefs.GetInt("resolution", -1) == -1)
        {
            resolutionDropdown.value = currentResolutionIndex;
        }
        else
        {
            resolutionDropdown.value = PlayerPrefs.GetInt("resolution");
        }
        resolutionDropdown.RefreshShownValue();

        OnEnable();
        gameObject.SetActive(false);
    }
    private void OnEnable()
    {
        qualityDropdown.value = PlayerPrefs.GetInt("quality", 0);
        qualityDropdown.RefreshShownValue();

        fullScreenToggle.isOn = PlayerPrefs.GetInt("fullscreen", 1) == 1;
        SetFullscreen(PlayerPrefs.GetInt("fullscreen", 1) == 1);

        volumeSlider.value = PlayerPrefs.GetFloat("volume", 1);
        if (sensitivitySlider)
            sensitivitySlider.value = PlayerPrefs.GetFloat("sensitivity", 3);
    }
    public void SetResolution(int resolutionIndex)
    {
        Resolution resolution = resolutions[resolutionIndex];
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
        PlayerPrefs.SetInt("resolution", resolutionIndex);
    }

    public void SetVolume(float volume)
    {
        audioMixer.SetFloat("volume", volume);
        PlayerPrefs.SetFloat("volume", volume);
    }
    public void SetSensitivity(float s)
    {
        PlayerPrefs.SetFloat("sensitivity", s);
        if (LevelManager.Instance)
        {
            LevelManager.Instance.cameraSystem
                ?.GetComponentInChildren<CamPivotController>().UpdateSensitivity();
        }
    }
    public void SetQuality(int qualityIndex)
    {
        QualitySettings.SetQualityLevel(qualityIndex);
        PlayerPrefs.SetInt("quality", qualityIndex);
    }

    public void SetFullscreen(bool isFullscreen)
    {
        Screen.fullScreen = isFullscreen;
        PlayerPrefs.SetInt("fullscreen", isFullscreen ? 1 : 0);
    }
}
