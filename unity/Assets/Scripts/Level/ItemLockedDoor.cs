﻿using UnityEngine;
using System.Collections;

public class ItemLockedDoor : DoorTransport
{
    public string requiredItemName;
    public override void OnPress()
    {
        if (isLocked && Inventory.Instance.HasItem(requiredItemName))
        {
            Unlock();
            Inventory.Instance.Remove(requiredItemName);
        }
        base.OnPress();
    }
}
