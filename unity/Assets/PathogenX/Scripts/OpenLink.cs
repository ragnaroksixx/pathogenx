﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenLink : MonoBehaviour
{
    public void OpenItchio()
    {
        Application.OpenURL("https://sodaraptor.itch.io/");
    }
    public void OpenTwitter()
    {
        Application.OpenURL("https://twitter.com/sodaraptor_dev");
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
