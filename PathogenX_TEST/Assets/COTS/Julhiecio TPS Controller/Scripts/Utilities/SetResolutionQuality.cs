﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;

[AddComponentMenu("Julhiecio TPS Controller /Utilities/Resolution Quality")]
public class SetResolutionQuality : MonoBehaviour
{

    private Resolution start_current_resolution;
    [Space]
    [Header("is useful for increasing mobile performance")]
    [Header("this will reduce the resolution up to 3 times")]

    [Range(3,1)]
    public float ResolutionQuality;
    void Start()
    {
        //if has data of screen resolution
        if (PlayerPrefs.HasKey("screenresolutionWid") && PlayerPrefs.HasKey("screenresolutionHi"))
        {
            SetRenderResolutionQuality(PlayerPrefs.GetInt("screenresolutionWid"), PlayerPrefs.GetInt("screenresolutionHi"));
        }
        else
        {
            //save current resolution
            PlayerPrefs.SetInt("screenresolutionWid", Screen.currentResolution.width);
            PlayerPrefs.SetInt("screenresolutionHi", Screen.currentResolution.height);
            SetRenderResolutionQuality(Screen.currentResolution.width, Screen.currentResolution.height);
        }
    }
    private void SetRenderResolutionQuality(int width,int height)
    {
        //Load current resolution
        start_current_resolution.width = width;
        start_current_resolution.height = height;

        //divide the resolution
        int w = (int)((float)start_current_resolution.width / ResolutionQuality);
        int h = (int)((float)start_current_resolution.height / ResolutionQuality);

        //Set New Resolution
        Screen.SetResolution(w, h, true);
        print("Resolution: Width: " + w + "Height: " + h);
    }
}
