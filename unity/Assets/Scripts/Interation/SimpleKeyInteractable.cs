﻿using UnityEngine;
using System.Collections;

public abstract class SimpleKeyInteractable : Interactable
{
    protected bool inTrigger;
    public KeyCode keyPress;
    public override void OnEnter(Collider other)
    {
        inTrigger = true;
    }

    public override void OnExit(Collider other)
    {
        inTrigger = false;
    }
    private void Update()
    {
        if(inTrigger && Input.GetKeyDown(keyPress))
        {
            OnPress();
        }
    }
    public abstract void OnPress();
}
