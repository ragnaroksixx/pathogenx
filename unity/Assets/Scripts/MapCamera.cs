﻿using UnityEngine;
using System.Collections;
using System;

public class MapCamera : MonoBehaviour
{
    private void Awake()
    {
        Publisher.Subscribe<AreaEnterEvent>(OnAreaEnter);
    }
    private void OnDestroy()
    {
        Publisher.Unsubscribe<AreaEnterEvent>(OnAreaEnter);
    }

    private void OnAreaEnter(AreaEnterEvent e)
    {
        Vector3 pos = e.area.mapImage.transform.position;
        pos.y = transform.position.y;
        transform.position = pos;
    }
}
