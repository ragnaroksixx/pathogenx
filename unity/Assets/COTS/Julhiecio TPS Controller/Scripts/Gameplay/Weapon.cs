﻿using UnityEngine;
using UnityEditor;

[AddComponentMenu("Julhiecio TPS Controller /Gameplay/Weapon")]
[RequireComponent(typeof(AudioSource))]
public class Weapon : MonoBehaviour
{
    [Header("Weapon Setting")]
    public string WeaponName;
    public int WeaponID;
    public bool Unlocked;
    public WeaponType Type;
    [Space]

    [Range(1, 200)]
    public int BulletsPerMagazine = 10;
    public int TotalBullets = 150;
    public int BulletsAmounts = 10;

    [Space]
    public float Fire_Rate = 0.3f;
    [Range(0.1f, 50f)]
    public float Precision = 0.5f;
    [Range(0.01f, 1f)]
    public float LossOfAccuracyPerShot = 1;
    public GameObject BulletPrefab;
    public GameObject MuzzleFlashParticlePrefab;
    public Transform Shoot_Position;

    [Space]
    [Header("Generate procedural recoil animation")]
    public bool GenerateProceduralAnimation = true;
    [Range(0f, 0.3f)]
    public float RecoilForce = 0.1f;
    [Range(0f, 100)]
    public float RecoilForceRotation = 20;
    public Transform GunSlider;
    [Range(0f, 0.1f)]
    public float SliderMovementOffset;
    [HideInInspector] public Vector3 SliderStartLocalPosition;
    [Space]
    [Header("Bullet Casing Emitter")]
    public GameObject BulletCasingPrefab;

    [Space]
    [Tooltip("This is where the left hand will be")]
    [Header("IK Settings")]
    public Transform IK_Position_LeftHand;

    [Space]
    [Header("Weapon Sounds")]
    public AudioClip ShootAudio;
    public AudioClip ReloadAudio;

    public int damage=10;
    public enum WeaponType
    {
        Rifle,
        Pistol,
        Shotgun,
        Flamethrower,
        PistolSpecial,
        Rocket,
    }

#if UNITY_EDITOR
    [Header("Gizmo View")]
    public GizmosSettings GizmosVisualizationSettings;

    private void LoadVisualizationAssets()
    {
        GizmosVisualizationSettings.ResourcesPath = Application.dataPath + "/Julhiecio TPS Controller/Editor/GizmosModels/";
        var path_to_load = GizmosVisualizationSettings.ResourcesPath;
        path_to_load = FileUtil.GetProjectRelativePath(path_to_load);

        GizmosVisualizationSettings.HandVisualizerWireMesh =
         AssetDatabase.LoadAssetAtPath<MeshFilter>(path_to_load + "Hand Visualizer Wire Model.fbx").sharedMesh;
        GizmosVisualizationSettings.HandVisualizerMesh =
         AssetDatabase.LoadAssetAtPath<MeshFilter>(path_to_load + "Hand Visualizer Model.fbx").sharedMesh;
    }

    void OnDrawGizmos()
    {
        float mscale = .1f;
        if (GizmosVisualizationSettings.HandVisualizerMesh == null)
            LoadVisualizationAssets();
        if (GizmosVisualizationSettings.HandVisualizerMesh != null)
        {
            var pos = IK_Position_LeftHand.position + IK_Position_LeftHand.forward * 0.1f + IK_Position_LeftHand.up * -0.03f;
            var rot = IK_Position_LeftHand.rotation;

            Gizmos.color = new Color(0.9F, 0.6F, 0.3F, 1F);
            Gizmos.DrawWireMesh(GizmosVisualizationSettings.HandVisualizerWireMesh, pos, rot, new Vector3(-mscale, mscale, mscale));
            Gizmos.color = new Color(0, 0, 0, .3F);
            Gizmos.DrawMesh(GizmosVisualizationSettings.HandVisualizerMesh, pos, rot, new Vector3(-mscale, mscale, mscale));

        }
        if (Shoot_Position != null)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(Shoot_Position.position, 0.02f);
            Gizmos.DrawLine(Shoot_Position.position, Shoot_Position.position + Shoot_Position.forward * 0.5f);
        }
    }
#endif
}