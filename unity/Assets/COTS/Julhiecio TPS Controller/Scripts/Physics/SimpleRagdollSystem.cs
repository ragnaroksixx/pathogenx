﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEditor;
using UnityEngine;
[AddComponentMenu("Julhiecio TPS Controller /Physics/Ragdoll Controller")]
public class SimpleRagdollSystem : MonoBehaviour
{
    [Space]
    [Space]
    [Header("Create > 3D Object > Ragdoll...")]
    [Header("you have to create the ragdoll manually in:")]
    [Header("Warning: for this to work")]
    public Rigidbody[] RagdollBones;
    public bool IsActiveRagdoll;

    public void EnableRagdollPhysics()
    {
        for (int i = 0; i < RagdollBones.Length; i++)
        {
            if (this.gameObject != RagdollBones[i].gameObject)
            {
                RagdollBones[i].GetComponent<Rigidbody>().isKinematic = false;
            }
        }
        IsActiveRagdoll = true;
        GetComponent<Animator>().enabled = false;
    }
    public void DisableRagdollPhysics()
    {

        for (int i = 0; i < RagdollBones.Length; i++)
        {
            if (this.gameObject != RagdollBones[i].gameObject)
            {
                RagdollBones[i].GetComponent<Rigidbody>().isKinematic = true;
            }
        }
        IsActiveRagdoll = false;
        GetComponent<Animator>().enabled = true;
    }

    public void SeachRagdollBones()
    {
        RagdollBones = gameObject.GetComponentsInChildren<Rigidbody>();
        print(RagdollBones.Length +" ragdoll bones found");
        if (RagdollBones != null)
        {
            for (int i = 0; i < RagdollBones.Length; i++)
            {
                print(RagdollBones[i].name);
            }
        }
    }
}
#if UNITY_EDITOR
[CustomEditor(typeof(SimpleRagdollSystem))]
public class RagdollEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        SimpleRagdollSystem rag = (SimpleRagdollSystem)target;
        if (GUILayout.Button("Seach for Ragdoll Bones"))
        {
            rag.SeachRagdollBones();
        }
        if (GUILayout.Button("Disable Ragdoll Physics"))
        {
            rag.DisableRagdollPhysics();
        }
        if (GUILayout.Button("Enable Ragdoll Physics"))
        {
            rag.EnableRagdollPhysics();
        }
    }
}

#endif
