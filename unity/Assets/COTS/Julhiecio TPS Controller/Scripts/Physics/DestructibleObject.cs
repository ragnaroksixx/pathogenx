﻿using UnityEngine;

[AddComponentMenu("Julhiecio TPS Controller/Physics/Destructible")]
public class DestructibleObject : MonoBehaviour
{
    [Range(0,50)]
    public float Strength;
    public GameObject FracturedObject;
    public Vector3 PositionOffset;
    public float TimeToDestroy = 15;
    private bool IsFractured = false;

    public void _DestroyObject()
    {
        if (IsFractured == true)
            return;
        if (FracturedObject != null)
        {
            var fractured_obj = (GameObject)Instantiate(FracturedObject, transform.position + PositionOffset, transform.rotation);
            Destroy(this.gameObject, 0.05f);
            Destroy(fractured_obj, TimeToDestroy);
            IsFractured = true;
        }
        else
        {
            Debug.LogWarning("There is no 'Fractured Object' linked in " + gameObject.name);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Bullet")
        {
            _DestroyObject();
        }

    }
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Bullet")
        {
            _DestroyObject();
        }
        if (other.gameObject.TryGetComponent(out Rigidbody rb))
        {
            if (rb.velocity.magnitude > 5f)
            {
                _DestroyObject();
            }
        }
    }
    private void OnCollisionStay(Collision other)
    {
        if (other.gameObject.tag == "Bullet")
        {
            _DestroyObject();
        }
    }
}
