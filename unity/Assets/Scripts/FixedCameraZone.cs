﻿using UnityEngine;
using System.Collections;

public class FixedCameraZone : MonoBehaviour
{
    public GameObject cameraLocation;
    public static Transform currentFixedCamera;
    private void Awake()
    {
        Destroy(GetComponentInChildren<Camera>());
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            currentFixedCamera = cameraLocation.transform;
            Publisher.Raise(new FixedCameraEvent(cameraLocation.transform));
        }
    }
}

public class FixedCameraEvent : PublisherEvent
{
    public Transform location;

    public FixedCameraEvent(Transform location)
    {
        this.location = location;
    }
}
