﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Menu : MonoBehaviour
{
    CanvasGroup canvasGroup;
    public bool startVisible;
    protected virtual void Awake()
    {
        (transform as RectTransform).anchoredPosition = Vector2.zero;
        canvasGroup = GetComponent<CanvasGroup>();
    }
    protected virtual void Start()
    {
        if (startVisible)
            Show(true);
        else
            Hide(true);
    }

    public virtual void Show(bool instant = false)
    {
        canvasGroup.DOKill();
        if (instant)
        {
            canvasGroup.alpha = 1;
            gameObject.SetActive(true);
            canvasGroup.blocksRaycasts = canvasGroup.interactable = true;
            return;
        }
        gameObject.SetActive(true);
        canvasGroup.DOFade(1, 0.1f)
            .SetUpdate(true)
            .OnComplete(() => { canvasGroup.blocksRaycasts = canvasGroup.interactable = true; });
    }
    public virtual void Hide(bool instant = false)
    {
        canvasGroup.DOKill();
        canvasGroup.blocksRaycasts = canvasGroup.interactable = false;
        if (instant)
        {
            canvasGroup.alpha = 0;
            gameObject.SetActive(false);
            return;
        }
        canvasGroup.DOFade(1, 0.1f)
            .SetUpdate(true)
            .OnComplete(() => { gameObject.SetActive(false); });
    }
}
