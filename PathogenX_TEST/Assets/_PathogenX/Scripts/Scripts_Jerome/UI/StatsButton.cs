﻿using UnityEngine;
using System.Collections;
using TMPro;

public class StatsButton : MonoBehaviour
{
    public string titleText, key;
    public TMP_Text text, title;
    public bool isTime, isRank;
    private void Awake()
    {
        title.text = titleText;
        Publisher.Subscribe<UpdateStatUIEvent>(UpdateStatUI);
    }
    private void OnDestroy()
    {
        Publisher.Unsubscribe<UpdateStatUIEvent>(UpdateStatUI);
    }

    void UpdateStatUI(UpdateStatUIEvent e)
    {
        text.text = "0";
        if (WinMenu.stats.ContainsKey(key))
        {
            if (isTime)
            {
                int seconds = WinMenu.stats[key];
                int min = seconds / 60;
                seconds = seconds % 60;
                text.text = min.ToString("00") + ":" + seconds.ToString("00");
            }
            else if (isRank)
            {
                int X = 40000;
                int S = 30000;
                int A = 20000;
                int B = 10000;
                int C = 8000;
                int D = 6000;
                int score = WinMenu.stats[key];
                string result = "F  'Pigeon'";
                if (score >= X) result = "X  'Legend'";
                else if (score >= X) result = "X  'Legend'";
                else if (score >= S) result = "S  'Falcon'";
                else if (score >= A) result = "A  'Jackal'";
                else if (score >= B) result = "B  'Rabbit'";
                else if (score >= C) result = "C  'Iguana'";
                else if (score >= D) result = "D  'Salmon'";
                text.text = result;
            }
            else
            {
                text.text = WinMenu.stats[key].ToString();
            }
        }
    }
}
