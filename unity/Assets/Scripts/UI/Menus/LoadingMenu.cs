﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;
using System;
using System.Collections.Generic;
using DG.Tweening;

public class LoadingMenu : Menu
{
    public TMP_Text infoText;
    public Image loadingBar;
    float duration = 0.25f;
    protected override void Awake()
    {
        base.Awake();
        Publisher.Subscribe<LoadingEvent>(UpdateLoading);
    }
    private void OnDestroy()
    {
        Publisher.Unsubscribe<LoadingEvent>(UpdateLoading);
    }
    public void UpdateLoading(LoadingEvent e)
    {
        if (e.normalizedBar == 0)
        {
            infoText.text = e.text;
            loadingBar.fillAmount = 0;
            Show(true);

        }
        else if (e.normalizedBar == 1)
        {
            infoText.text = e.text;
            loadingBar.DOFillAmount(1, duration);
            Hide();
        }
        else
        {
            infoText.text = e.text;
            loadingBar.DOFillAmount(e.normalizedBar, duration);
        }
    }
}

public class LoadingEvent : PublisherEvent
{
    public string text;
    public float normalizedBar;

    public LoadingEvent(string text, float normalizedBar)
    {
        this.text = text;
        this.normalizedBar = normalizedBar;
    }
}

