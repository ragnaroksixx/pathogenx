﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "Area Data", menuName = "Level/AreaData")]
public class AreaData : ScriptableObject
{
    public GameObject prefab;
    public bool ignoreBossSpawn;

    public List<AreaData> invalidNextRooms;

    Area area;
    public Area Area
    {
        get
        {
            if (area == null)
                area = prefab.GetComponentInChildren<Area>();
            return area;
        }
    }

}

