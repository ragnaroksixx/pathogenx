﻿using UnityEngine;
using System.Collections;

public class MeleeAI : BasicAI
{
    public HitBox hb;
    public override void Awake()
    {
        hb.gameObject.SetActive(false);
        base.Awake();
    }
    protected override void OnEnable()
    {
        hb.gameObject.SetActive(false);
        base.OnEnable();
    }
    protected override void OnDisable()
    {
        hb.gameObject.SetActive(false);
        base.OnDisable();
    }
    public override void Attack()
    {
        base.Attack();
        hb.gameObject.SetActive(true);
    }
    public override void AttackEnd()
    {
        base.AttackEnd();
        hb.gameObject.SetActive(false);
    }
    public override void OnDie()
    {
        base.OnDie();
        hb.gameObject.SetActive(false);
    }
}

