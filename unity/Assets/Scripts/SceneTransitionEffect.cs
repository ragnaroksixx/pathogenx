﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneTransitionEffect : MonoBehaviour
{
    public CanvasGroup image;
    float transitionTime = 1;
    public static float waitTime = 0;
    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 0;
        Sequence s = DOTween.Sequence()
            .AppendInterval(waitTime)
            .AppendCallback(StartScene)
            .AppendCallback(() => { image.enabled = true; })
            .Append(image.DOFade(0, transitionTime))
            .AppendInterval(transitionTime)
            .OnComplete(OnComplete)
            .SetUpdate(true);

        s.Play();

    }
    void StartScene()
    {
        Time.timeScale = 1;
        waitTime = 0;
    }
    void OnComplete()
    {
        SceneController.UnloadAreaTransition();
    }
}
