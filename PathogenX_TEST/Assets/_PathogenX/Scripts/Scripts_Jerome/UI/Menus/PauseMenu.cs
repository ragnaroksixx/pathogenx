﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;
using System;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class PauseMenu : Menu
{
    bool IsPaused;
    public GameObject actionButtonsRoot;
    public ItemActionButton[] actionButtons;
    public TMP_Text selectedItemName, selectedItemDescription;
    public GameObject areYouSure;
    public TMP_Text areYouSureText;
    public Button yes;
    public Image heart;
    public Gradient healthColorGradient;
    protected override void Awake()
    {
        base.Awake();
        Publisher.Subscribe<PauseEvent>(OnPasueEvent);
        Publisher.Subscribe<SelectItemEvent>(OnSelectItem);
        Publisher.Subscribe<ConfirmationEvent>(OnConfirmation);
        Publisher.Subscribe<UpdateItemUIEvent>(OnUpdate);
        actionButtonsRoot.SetActive(false);
        selectedItemName.text = "";
        selectedItemDescription.text = "";
    }

    private void OnConfirmation(ConfirmationEvent e)
    {
        yes.onClick.RemoveAllListeners();
        yes.onClick.AddListener(
            () =>
            {
                e.onYes.Invoke();
                areYouSure.gameObject.SetActive(false);
            });
        areYouSureText.text = e.text;
        areYouSure.gameObject.SetActive(true);
    }

    private void OnSelectItem(SelectItemEvent e)
    {
        actionButtonsRoot.SetActive(e.item != null);
        if (e.item != null)
        {
            selectedItemName.text = e.item.displayName;
            selectedItemDescription.text = e.item.description;
            List<ItemAction> actions = e.item.GetActions();

            for (int i = 0; i < actionButtons.Length; i++)
            {
                actionButtons[i].Set
                    (i < actions.Count ? actions[i] : null);
            }
        }
        else
        {
            selectedItemName.text = "";
            selectedItemDescription.text = "";
        }
    }

    private void OnDestroy()
    {
        Publisher.Unsubscribe<PauseEvent>(OnPasueEvent);
        Publisher.Unsubscribe<SelectItemEvent>(OnSelectItem);
        Publisher.Unsubscribe<ConfirmationEvent>(OnConfirmation);
        Publisher.Unsubscribe<UpdateItemUIEvent>(OnUpdate);
    }

    public void EndRun()
    {
        Publisher.Raise(new ConfirmationEvent("Return to the main menu?"
    , () =>
    {
        AudioSystem.PlayBGM(null, Vector3.zero);
        SceneController.LoadInitScene();
        Hide(true);
    }));

    }
    public void UnPauseButton()
    {
        TimeHandler.Instance.UnPause();
    }
    public void OnPasueEvent(PauseEvent e)
    {
        if (e.isPaused)
            Show();
        else
            Hide();
    }
    public override void Show(bool instant = false)
    {
        base.Show(instant);
        MapCamera.Instance?.ResetPos();
        heart.color = healthColorGradient.Evaluate(Player.Instance.health.Normalized());
    }
    public override void Hide(bool instant = false)
    {
        Publisher.Raise(new SelectItemEvent(null));
        areYouSure.gameObject.SetActive(false);
        base.Hide(instant);
    }
    public void OnUpdate(UpdateItemUIEvent e)
    {
        heart.color = healthColorGradient.Evaluate(Player.Instance.health.Normalized());
    }
    public void OnDrag(BaseEventData e)
    {
        if (MapCamera.Instance)
        {
            PointerEventData pointer = (PointerEventData)e;
            Vector3 delta = Vector3.zero;
            delta.x -= pointer.delta.x * .25f;
            delta.z -= pointer.delta.y * .25f;
            MapCamera.Instance.transform.localPosition += delta;
        }
    }
}
[System.Serializable]
public class ItemActionButton
{
    public Button button;
    public TMP_Text text;

    public void Set(ItemAction ia)
    {
        if (ia == null)
        {
            button.onClick.RemoveAllListeners();
            text.text = "";
            button.gameObject.SetActive(false);
        }
        else
        {
            button.onClick.RemoveAllListeners();
            button.onClick.AddListener(ia.action.Invoke);
            text.text = ia.title;
            button.gameObject.SetActive(true);
        }
    }
}
public class ItemAction
{
    public string title;
    public Action action;

    public ItemAction(string title, Action action)
    {
        this.title = title;
        this.action = action;
    }
}
public class PauseEvent : PublisherEvent
{
    public bool isPaused;

    public PauseEvent(bool isPaused)
    {
        this.isPaused = isPaused;
    }
}
public class SelectItemEvent : PublisherEvent
{
    public Item item;

    public SelectItemEvent(Item i)
    {
        this.item = i;
    }
}
public class ConfirmationEvent : PublisherEvent
{
    public string text;
    public Action onYes;

    public ConfirmationEvent(string text, Action onYes)
    {
        this.text = text;
        this.onYes = onYes;
    }
}
