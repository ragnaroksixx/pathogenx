﻿using UnityEngine;
using System.Collections;

public class ApplicationInit : MonoBehaviour
{
    public string[] initialScenes;
    private void Awake()
    {
        SceneController.SetInitScene();
    }
    private void Start()
    {
        SceneController.LoadScenesAdditive(initialScenes);
    }
}
