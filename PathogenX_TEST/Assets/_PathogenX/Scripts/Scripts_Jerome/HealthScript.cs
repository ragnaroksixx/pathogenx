﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Events;

public class HealthScript : MonoBehaviour
{
    public int maxHP;
    public int currentHP;
    public UnityEvent onDieEvent, onDamageEvent;
    public int deathScore;
    public GameObject deathParticles;
    public virtual void Awake()
    {
        currentHP = maxHP;
    }
    public virtual void TakeDamage(int amount)
    {
        if (currentHP <= 0) return;
        currentHP -= amount;
        if (currentHP <= 0)
            Die();
        else
            onDamageEvent?.Invoke();
    }
    public virtual void Heal(int amount)
    {
        currentHP = Mathf.Max(currentHP + amount, maxHP);
    }
    public float Normalized()
    {
        return (float)currentHP / maxHP;
    }
    protected virtual void Die()
    {
        onDieEvent?.Invoke();
        if (deathParticles)
            GameObject.Instantiate(deathParticles, transform.position, Quaternion.identity);
        if (deathScore > 0)
            WinMenu.AddStat("score", GetComponentInChildren<HealthScript>().deathScore);
    }
}
