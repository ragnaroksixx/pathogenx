﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WinMenu : Menu
{
    public static Dictionary<string, int> stats = new Dictionary<string, int>();
    protected override void Awake()
    {
        base.Awake();
        Publisher.Subscribe<ShowStatsEvent>(ShowStats);
        Publisher.Subscribe<SetStatEvent>(SetStat);
        Publisher.Subscribe<ClearAllStatsEvent>(ClearAll);
    }
    private void OnDestroy()
    {
        Publisher.Unsubscribe<ShowStatsEvent>(ShowStats);
        Publisher.Unsubscribe<SetStatEvent>(SetStat);
        Publisher.Unsubscribe<ClearAllStatsEvent>(ClearAll);
    }
    void SetStat(SetStatEvent e)
    {
        if (stats.ContainsKey(e.name))
        {
            stats[e.name] += e.toAdd;
        }
        else
        {
            stats.Add(e.name, e.toAdd);
        }
        Publisher.Raise(new UpdateStatUIEvent());
    }
    void ClearAll(ClearAllStatsEvent e)
    {
        stats.Clear();
        Publisher.Raise(new UpdateStatUIEvent());
    }
    public static void AddStat(string key, int val)
    {
        Publisher.Raise(new SetStatEvent(key, val));
    }
    public void ShowStats(ShowStatsEvent e)
    {
        Show();
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }
    public void MainMenu()
    {
        SceneController.LoadInitScene();
        Hide();
    }
}

public class SetStatEvent : PublisherEvent
{
    public string name;
    public int toAdd;

    public SetStatEvent(string name, int toAdd)
    {
        this.name = name;
        this.toAdd = toAdd;
    }
}
public class UpdateStatUIEvent : PublisherEvent
{

}
public class ClearAllStatsEvent : PublisherEvent
{
}
public class ShowStatsEvent : PublisherEvent
{
}