﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTrigger : MonoBehaviour
{
    public GameObject myCamera;
    private CameraManager myCameraManager;

    // Start is called before the first frame update
    void Start()
    {
        myCameraManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<CameraManager>();
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            myCameraManager.DeactivateAllCameras();
            myCamera.SetActive(true);
        }
    }

}
