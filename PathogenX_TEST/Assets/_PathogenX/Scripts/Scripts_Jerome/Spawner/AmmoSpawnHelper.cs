﻿using UnityEngine;
using System.Collections;

public class AmmoSpawnHelper : SpawnHelper
{
    public override float GetSpawnMultiplier(SpawnData sd)
    {
        float val = base.GetSpawnMultiplier(sd);
        AmmoBox aBox = sd.obj.GetComponentInChildren<AmmoBox>();
        if (aBox)
        {
            val = HasGun(aBox);
        }
        else if (sd.obj.name.Contains("empty"))
        {
            return Mathf.Min(TotalAmmo(), 0.9f);
        }
        return val;
    }
    public float TotalAmmo()
    {
        float ammo = 0;
        float ceiling = 0;
        for (int i = 0; i < Inventory.Instance.player.Weapons.Length; i++)
        {
            if (!Inventory.Instance.player.Weapons[i].Unlocked) continue;
            ceiling += Inventory.Instance.player.Weapons[i].BulletsPerMagazine;
            ammo += Inventory.Instance.player.Weapons[i].TotalBullets;
            ammo += Inventory.Instance.player.Weapons[i].BulletsAmounts;
        }
        return Mathf.Max(ammo / ceiling, 1);
    }
    public float HasGun(AmmoBox a)
    {
        for (int i = 0; i < Inventory.Instance.player.Weapons.Length; i++)
        {
            switch (Inventory.Instance.player.Weapons[i].Type)
            {
                case Weapon.WeaponType.Rifle:
                    if (a.AmmoType != AmmoBox.TypeOfAmmo.Rifle) continue;
                    break;
                case Weapon.WeaponType.Pistol:
                    if (a.AmmoType != AmmoBox.TypeOfAmmo.Pistol) continue;
                    break;
                case Weapon.WeaponType.Shotgun:
                    if (a.AmmoType != AmmoBox.TypeOfAmmo.Shotgun) continue;
                    break;
                default:
                    continue;
            }
            if (Inventory.Instance.player.Weapons[i].Unlocked)
            {
                if (Inventory.Instance.player.Weapons[i].TotalBullets > 0)// Inventory.Instance.player.Weapons[i].BulletsPerMagazine)
                {
                    return 1.0f;
                }
                else
                {
                    return 1.2f;
                }
            }
        }
        return 0.8f;
    }
}
