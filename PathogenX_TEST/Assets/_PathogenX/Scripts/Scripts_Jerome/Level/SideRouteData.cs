﻿using UnityEngine;
using System.Collections;
[CreateAssetMenu(fileName = "Side Level Data", menuName = "Level/Side LevelData")]

public class SideRouteData : ScriptableObject
{
    public AreaData branchingRoom, endRoom;
    public AreaData[] possibleRooms;
    public int minRooms, maxRooms;
    public int minToStart, maxToStart;
    [Range(0,1)]
    public float occurenceRate; //1 is required
    public int maxOccurences;
}

