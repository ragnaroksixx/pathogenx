﻿using UnityEngine;
using System.Collections;
[CreateAssetMenu(fileName = "Level Data", menuName = "Level/LevelData")]
public class LevelData : ScriptableObject
{
    public AreaData startingArea;
    public AreaData[] possibleRooms;
    public int[] minRoomsToEnd;
    public AreaData deadEndHoriz;
    public AreaData midPoint;
    public AreaData objPoint;
    public CustomAudioClip bgm;
    public GameObject player;

    public SideRouteData[] requiredSideRoutes_1;
    public SideRouteData[] requiredSideRoutes_2;
    public SideRouteData[] optionalSideRoutes;

}
