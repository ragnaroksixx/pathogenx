﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{
    public static Player Instance;
    public PlayerHealth health;
    private void Awake()
    {
        Instance = this;
    }
    private void OnDestroy()
    {
        Instance = null;
    }
}
