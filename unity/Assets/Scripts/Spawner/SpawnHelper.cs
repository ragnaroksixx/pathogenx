﻿using UnityEngine;
using System.Collections;

public class SpawnHelper : MonoBehaviour
{
    public virtual float GetSpawnMultiplier(SpawnData sd)
    {
        return 1;
    }
}
