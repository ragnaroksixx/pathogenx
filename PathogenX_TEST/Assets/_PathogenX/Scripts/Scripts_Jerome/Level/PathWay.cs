﻿using UnityEngine;
using System.Collections;
using System;

public class PathWay : MonoBehaviour
{
    PathWay connectedPathway = null;
    public bool HasConnectedRoom { get => connectedPathway != null; }
    private void OnValidate()
    {
        connectedPathway = null;
    }
    public void ConnectArea(Area a, PathWay path)
    {
        if (HasConnectedRoom)
            throw new Exception("Area already connected to this path");
        Link(this, path);
        PositionRoomAtDoorway(a, path);
    }
    public void Link(PathWay a, PathWay b)
    {
        a.connectedPathway = b;
        b.connectedPathway = a;
    }
    public void Unlink()
    {
        if (HasConnectedRoom)
        {
            connectedPathway.connectedPathway = null;
        }
    }
    public bool CanConnectRoom(Area areaPrefab, PathWay pathOfPrefab)
    {
        Quaternion rot = CalcConnectRoomRotation(pathOfPrefab);
        Vector3 center = CalcConnectRoomPositionPrefab(areaPrefab, pathOfPrefab, rot);
        Vector3 extents = (areaPrefab.boundsCol.size / 2) * 0.9f;
        Collider[] cols = Physics.OverlapBox(center, extents, rot);
        return cols.Length == 0;
    }

    public void PositionRoomAtDoorway(Area area, PathWay path)
    {
        // Reset room position and rotation
        area.rootObj.position = Vector3.zero;
        area.rootObj.rotation = Quaternion.identity;

        // Rotate room to match previous doorway orientation
        area.rootObj.rotation = CalcConnectRoomRotation(path);

        // Position room
        area.rootObj.position = CalcConnectRoomPosition(area, path);
    }
    public Vector3 CalcConnectRoomPosition(Area area, PathWay door)
    {
        Vector3 roomPositionOffset = door.transform.position - area.rootObj.position;
        return transform.position - roomPositionOffset;
    }
    public Vector3 CalcConnectRoomPositionPrefab(Area area, PathWay door, Quaternion rot)
    {
        Vector3 roomPositionOffset = door.transform.position - area.rootObj.position;
        roomPositionOffset = rot * roomPositionOffset;
        return transform.position - roomPositionOffset;
    }
    public Quaternion CalcConnectRoomRotation(PathWay door)
    {
        Vector3 targetDoorwayEuler = transform.eulerAngles;
        Vector3 roomDoorwayEuler = door.transform.eulerAngles;
        float deltaAngle = Mathf.DeltaAngle(roomDoorwayEuler.y, targetDoorwayEuler.y);
        Quaternion currentRoomTargetRotation;
        currentRoomTargetRotation = Quaternion.AngleAxis(deltaAngle, Vector3.up);
        return currentRoomTargetRotation * Quaternion.Euler(0, 180f, 0);
    }
}
