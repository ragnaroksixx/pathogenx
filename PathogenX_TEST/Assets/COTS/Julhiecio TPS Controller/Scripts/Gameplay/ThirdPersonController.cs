﻿using UnityEngine;

[AddComponentMenu("Julhiecio TPS Controller /Gameplay/Player Controller")]
[RequireComponent(typeof(Rigidbody), typeof(AudioSource), typeof(CapsuleCollider))]
public class ThirdPersonController : MonoBehaviour
{
    [HideInInspector] public Animator anim;
    [HideInInspector] Rigidbody rb;
    [HideInInspector] Camera myCamera;
    [HideInInspector] CamPivotController MyPivotCamera;

    Transform MyCamera
    {
        get
        {
            return lastCameraTransform.transform;
        }
    }
    public Transform lastCameraTransform;

    [Header("Movement Settings")]
    public float Velocity = 5;
    public float runSpeedMult = 1.25f;

    public float JumpForce = 2.5f;
    public float AirInfluenceControll = 0.5f;
    public Transform LookAtDirection;

    float VelocityMultiplier;
    float VerticalY;
    float HorizontalX;
    [HideInInspector] float BodyRotation;
    Vector3 Rotation;
    [HideInInspector] Vector3 EulerRotation;
    [HideInInspector] float LastX, LastY, LastVelMult;
    [HideInInspector] Quaternion eul;

    [Header("Mobile Movement")]
    public bool IsMobile = false;
    private JoystickVirtual MovementJoystick;
    //Buttons
    private ButtonVirtual ShotButton, ReloadButton, RunButton, JumpButton,
         CrouchButton, RollButton, PickWeaponButton, EnterVehicleButton, NextWeaponButton, PreviousWeaponButton;

    //Input Bools
    private bool PressedShooting, PressedReload, PressedRun, PressedJump,
         PressedCrouch, PressedRoll, PressedPickupWeapon, PressedEnterVehicle, PressedNextWeapon, PressedPreviousWeapon;
    [Header("Physics")]
    public bool RagdollWhenDie;
    private SimpleRagdollSystem ragdollcontrol;

    [Header("Ground Check Settings")]
    public LayerMask WhatIsGround;
    public float GroundCheckRadious = 0.3f;
    public float GroundCheckHeighOfsset = 0f;
    public float GroundCheckSize = 1f;

    [Header("Step Settings")]
    public bool EnableStepCorrection = true;
    public LayerMask StepCorrectionMask;
    public float FootstepHeight = 0.8f;
    public float StepOffset = 0.33f;
    public float StepHeight = 0.12f;

    [Header("Footstep Settings")]
    public AudioClip[] FootstepAudioClips;
    public float MaxFootstepTime = 0.31f;
    [HideInInspector] float currentFootstepTime;
    [HideInInspector] AudioSource AudioS;

    [Header("Punch Attacks Settings")]
    public bool EnabledPunchAttacks;
    [HideInInspector] float currenttimetodisable_isattacking = 0;
    public GameObject PunchCollisionParticle;

    [Header("Weapons Settings")]
    public LayerMask CrosshairHitMask;
    public GameObject PivotWeapons;
    public Weapon[] Weapons;
    [HideInInspector] public Weapon WeaponInUse;
    private int WeaponID = -1; // [-1] = Hand
    [HideInInspector] float IsArmedWeight;
    [HideInInspector] float CurrentFireRateToShoot;
    [HideInInspector] public float ShotErrorProbability;
    [HideInInspector] RaycastHit CrosshairHit;

    [Header("(IK) Hands - Hand Position Target")]
    public Transform IKPositionRHAND;
    public Transform IKPositionLHAND;
    [Header("(IK) Hands In Weapons - Position")]
    public Transform Type_PistolIK_RHand_Pos;
    public Transform Type_RifleIK_RHand_Pos;

    Vector3 StartPistolIKPosition;
    Quaternion StartPistolIKRotation;
    Vector3 StartRifleIKPosition;
    Quaternion StartRifleIKRotation;

    [HideInInspector] public Transform HumanoidSpine;
    float WeightIK;

    [Header("Pick Up Weapons")]
    public LayerMask WeaponLayer;

    [HideInInspector]
    public Vehicle VehicleInArea;

    [Header("States")]
    public float Life = 100;
    public bool IsDead;
    [HideInInspector] public bool CanMove;
    [HideInInspector] public bool IsMoving;
    [HideInInspector] public bool IsRunning;
    [HideInInspector] public bool IsCrouched;
    public bool IsJumping;
    public bool IsGrounded;
    [HideInInspector] public bool IsAttacking;
    [HideInInspector] public bool IsArmed;
    [HideInInspector] public bool IsAimingSnipe;
    [HideInInspector] public bool ToPickupWeapon;
    [HideInInspector] public bool IsRolling;
    [HideInInspector] public bool IsDriving;
    public bool ToEnterVehicle;
    [HideInInspector] public bool Shot;
    [HideInInspector] public bool CanShoot;
    [HideInInspector] public bool WallInFront;
    [HideInInspector] public bool InverseKinematics = true;


    void Start()
    {
        WeaponID = -1;
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
        AudioS = GetComponent<AudioSource>();
        myCamera = Camera.main;
        lastCameraTransform = myCamera.transform;
        MyPivotCamera = FindObjectOfType<CamPivotController>();
        WeaponInUse = null;
        HumanoidSpine = anim.GetBoneTransform(HumanBodyBones.UpperChest).transform;
        if (RagdollWhenDie)
        {
            ragdollcontrol = GetComponent<SimpleRagdollSystem>();
        }
        //MOBILE INPUTS
        if (IsMobile)
        {
            //Controll Buttons
            MovementJoystick = FindObjectOfType<JoystickVirtual>();
            ShotButton = GameObject.Find("ShotButton").GetComponent<ButtonVirtual>();
            JumpButton = GameObject.Find("JumpButton").GetComponent<ButtonVirtual>();
            RunButton = GameObject.Find("RunButton").GetComponent<ButtonVirtual>();
            RollButton = GameObject.Find("RollButton").GetComponent<ButtonVirtual>();
            CrouchButton = GameObject.Find("CrouchButton").GetComponent<ButtonVirtual>();
            ReloadButton = GameObject.Find("ReloadButton").GetComponent<ButtonVirtual>();

            //Interact Buttons
            PickWeaponButton = GameObject.Find("WeaponButton").GetComponent<ButtonVirtual>();
            EnterVehicleButton = GameObject.Find("VehicleButton").GetComponent<ButtonVirtual>();
            //Weapon Switch
            PreviousWeaponButton = GameObject.Find("PreviousWeaponButton").GetComponent<ButtonVirtual>();
            NextWeaponButton = GameObject.Find("NextWeaponButton").GetComponent<ButtonVirtual>();
        }

        StartPistolIKPosition = Type_PistolIK_RHand_Pos.localPosition;
        StartPistolIKRotation = Type_PistolIK_RHand_Pos.localRotation;
        StartRifleIKPosition = Type_RifleIK_RHand_Pos.localPosition;
        StartRifleIKRotation = Type_RifleIK_RHand_Pos.localRotation;

        for (int i = 0; i < Weapons.Length; i++)
        {
            Weapons[i].gameObject.layer = 0;
            Weapons[i].GetComponent<Collider>().enabled = false;
            Weapons[i].gameObject.SetActive(false);
        }
    }
    void FixedUpdate()
    {
        if (TimeHandler.Instance.isPaused)
            return;
        if (IsDead == true)
            return;
        Movement();
        StepCorrect();
    }
    void Update()
    {
        if (TimeHandler.Instance.isPaused)
            return;
        if (IsDead == true)
        {
            Life = 0;
            CanMove = false;
            IsRunning = false;
            IsCrouched = false;
            IsJumping = false;
            IsGrounded = false;
            IsArmed = false;
            IsRolling = false;
            IsDriving = false;
            Shot = false;
            shotRelaseTime = Time.time;
            WallInFront = false;
            InverseKinematics = false;
            anim.SetLayerWeight(1, 0);

            var rot = transform.rotation;
            rot.x = 0;
            rot.z = 0;
            transform.rotation = rot;

            GetComponent<Collider>().isTrigger = false;
            rb.useGravity = true;
            rb.velocity = transform.up * rb.velocity.y;

            //if (ragdollcontrol.IsActiveRagdoll == false)
            //{
            //    ragdollcontrol.EnableRagdollPhysics();
            //}
            return;
        }


        if (IsDead == false)
        {
            Rotate();
            Footsteps();
            WeaponControll();
            DriveControll();
            PickUpWeapons();
            WeaponIKControll();
            Inputs();

            if (IsMobile == false)
                return;
            MobileInputs();
        }
    }

    #region Weapon Switch
    private void _NextWeapon()
    {
        SwitchWeapons(SwitchDirection.Forward);
    }
    private void _PreviousWeapon()
    {
        SwitchWeapons(SwitchDirection.Backward);
    }

    private enum SwitchDirection { Forward, Backward }
    void SwitchWeapons(SwitchDirection Direction)
    {
        PivotWeapons.transform.localEulerAngles = new Vector3(PivotWeapons.transform.localEulerAngles.x + 30, 0, 0);
        WeightIK = 0.4f;

        if (Direction == SwitchDirection.Forward)
        {
            WeaponID++;
            for (int i = 0; i < Weapons.Length; i++)
            {
                if (i != WeaponID)
                {
                    Weapons[i].gameObject.SetActive(false);
                }
                else
                {
                    if (Weapons[i].Unlocked == true)
                    {
                        Weapons[i].gameObject.SetActive(true);
                        WeaponInUse = Weapons[i];
                    }
                    else
                    {
                        _NextWeapon();
                        WeaponInUse = null;
                    }
                }
            }
        }
        if (Direction == SwitchDirection.Backward)
        {
            WeaponID--;

            for (int i = Weapons.Length - 1; i > -1; i--)
            {
                if (i != WeaponID)
                {
                    Weapons[i].gameObject.SetActive(false);
                }
                else
                {
                    if (Weapons[i].Unlocked == true)
                    {
                        Weapons[i].gameObject.SetActive(true);
                        WeaponInUse = Weapons[i];
                    }
                    else
                    {
                        WeaponInUse = null;
                        _PreviousWeapon();
                    }
                }
            }
        }
        if (IsArmed)
        {
            IsAttacking = false;
            currenttimetodisable_isattacking = 0;
        }
        if (WeaponID == -2)
        {
            WeaponID = Weapons.Length;
        }
        if (WeaponID == Weapons.Length)
        {
            WeaponID = -1;
            WeaponInUse = null;
            IsArmed = false;
        }

        //NO WEAPONS
        if (WeaponID == -1)
        {
            WeaponInUse = null;
            InverseKinematics = false;
        }
        else
        {
            InverseKinematics = true;
        }
        Shot = false;
    }
    public void Equip(int id)
    {
        WeaponID = id;
        PivotWeapons.transform.localEulerAngles = new Vector3(PivotWeapons.transform.localEulerAngles.x + 30, 0, 0);
        WeightIK = 0.4f;

        for (int i = 0; i < Weapons.Length; i++)
        {
            if (i != WeaponID)
            {
                Weapons[i].gameObject.SetActive(false);
            }
            else
            {
                Weapons[i].gameObject.SetActive(true);
                WeaponInUse = Weapons[i];
            }
        }


        if (IsArmed)
        {
            IsAttacking = false;
            currenttimetodisable_isattacking = 0;
        }
        if (WeaponID == -2)
        {
            WeaponID = Weapons.Length;
        }
        if (WeaponID == Weapons.Length)
        {
            WeaponID = -1;
            WeaponInUse = null;
            IsArmed = false;
        }

        //NO WEAPONS
        if (WeaponID == -1)
        {
            WeaponInUse = null;
            InverseKinematics = false;
        }
        else
        {
            InverseKinematics = true;
        }
        Shot = false;
    }
    #endregion
    float shotRelaseTime = 0;
    public float rollInputDelay = 1;
    float rollInputTrack = 0;
    #region Actions
    void Inputs()
    {
        //Dying
        if (Life <= 0 && IsDead == false)
        {
            anim.SetTrigger("die");

            IsDead = true;
        }

        //Movement Inputs
        VerticalY = Input.GetAxis("Vertical");
        HorizontalX = Input.GetAxis("Horizontal");

        if (VerticalY > 1 || HorizontalX > 1)
        {
            VerticalY = Mathf.Clamp(VerticalY, 0, 1);
            HorizontalX = Mathf.Clamp(HorizontalX, 0, 1);
        }
        //GetTheCameraRotation
        eul = MyCamera.transform.rotation;
        eul.x = 0;
        eul.z = 0;

        //Crouch
        if (Input.GetKeyDown(KeyCode.C) || PressedCrouch == true && IsGrounded == true && IsRunning == false && IsDriving == false)
        {
            if (anim.GetBool("crouched") == false)
            {
                IsCrouched = true;
            }
            else
            {
                IsCrouched = false;
            }
            if (CrouchButton != null)
            {
                CrouchButton.IsPressed = false;
            }
        }
        anim.SetBool("crouched", IsCrouched);

        //Ground Check
        if (IsDriving == false)
        {
            var groundcheck = Physics.OverlapBox(transform.position + transform.up * GroundCheckHeighOfsset, new Vector3(GroundCheckRadious, GroundCheckSize, GroundCheckRadious), transform.rotation, WhatIsGround);
            if (groundcheck.Length != 0 && IsJumping == false)
            {
                IsGrounded = true;
            }
            else
            {
                IsGrounded = false;
            }
        }
        anim.SetBool("isgrounded", IsGrounded);
        anim.SetBool("jumping", IsJumping);
        //Wall in front
        RaycastHit HitFront;
        if (Physics.Raycast(transform.position + transform.up * 1f, LookAtDirection.forward, out HitFront, 0.6f, WhatIsGround))
        {
            WallInFront = true;
        }
        else
        {
            WallInFront = false;
        }
        if (WallInFront == true)
        {
            VelocityMultiplier = Mathf.Lerp(VelocityMultiplier, 0, 5 * Time.deltaTime);
        }

        //Jump Mobile
        if (PressedJump == true && IsGrounded == true && IsJumping == false && IsRolling == false && IsDriving == false)
        {
            rb.AddForce(transform.up * 200 * JumpForce, ForceMode.Impulse);
            IsGrounded = false;
            IsJumping = true;
            IsCrouched = false;
            if (JumpButton != null)
            {
                JumpButton.IsPressed = false;
            }
            Invoke("_disablejump", .5f);
        }
        //Jump Normal
        if (Input.GetButtonDown("Jump") && IsGrounded == true && IsJumping == false && IsRolling == false && IsDriving == false)
        {
            rb.AddForce(transform.up * 200 * JumpForce, ForceMode.Impulse);
            IsGrounded = false;
            IsJumping = true;
            IsCrouched = false;
            if (JumpButton != null)
            {
                JumpButton.IsPressed = false;
            }
            Invoke("_disablejump", .5f);
        }

        //Roll
        if (Time.time > rollInputTrack && (Input.GetKeyDown(KeyCode.Space) || PressedRoll) && IsGrounded == true && IsRolling == false && !Shot)
        {
            IsRolling = true;
            rollInputTrack = Time.time + rollInputDelay;
            anim.SetTrigger("roll");
            if (RollButton != null)
            {
                RollButton.IsPressed = false;
            }
            Invoke("_disableroll", 1f);
            Player.Instance.health.SetIFrames(1f);
            DisableDoorEvent.DelayDoor();
        }

        //Running
        if (Input.GetKey(KeyCode.LeftShift) || PressedRun)
        {
            IsRunning = true;
            IsCrouched = false;
        }
        else
        {
            IsRunning = false;
        }

        anim.SetBool("running", IsRunning);

        //Punch Attacks
        if (EnabledPunchAttacks)
        {
            if (IsMobile == true)
            {
                if (PressedShooting && IsRolling == false && IsDriving == false && IsArmed == false && WeaponInUse == null)
                {
                    PunchAttack();
                    ShotButton.IsPressed = false;
                }
            }
            else
            {
                if (Input.GetKeyDown(KeyCode.Mouse0) && IsRolling == false && IsDriving == false && IsArmed == false && WeaponInUse == null)
                {
                    PunchAttack();
                }
            }
        }
        //Is Attacking time counter
        if (IsAttacking == true)
        {
            currenttimetodisable_isattacking += Time.deltaTime;
            if (currenttimetodisable_isattacking >= 0.7f)
            {
                currenttimetodisable_isattacking = 0;
                IsAttacking = false;
            }
        }

        //Shot / Reload Inputs
        if (IsArmed && WeaponInUse != null)
        {
            if (IsMobile == false) //Block "Mouse0" Input
            {
                //Shot
                if (Input.GetKey(KeyCode.Mouse0) && IsRolling == false && IsDriving == false && CanShoot == true && WeaponInUse.BulletsAmounts > 0 && WeightIK > 0.9f)
                {
                    Shoot();
                }
                //Reload
                if (Input.GetKeyDown(KeyCode.R) && WeaponInUse.BulletsAmounts < WeaponInUse.BulletsPerMagazine && WeaponInUse.TotalBullets > 0)
                {
                    anim.SetTrigger("reload");
                    DisableDoorEvent.DelayDoor();
                }
            }
            else
            {
                //Shot by virtual button
                if (PressedShooting && IsRolling == false && IsDriving == false && CanShoot == true && WeaponInUse.BulletsAmounts > 0 && WeightIK > 0.9f)
                {
                    Shoot();
                }
                //Reload by virtual button
                if (Input.GetKeyDown(KeyCode.R) || PressedReload && WeaponInUse.BulletsAmounts < WeaponInUse.BulletsPerMagazine && WeaponInUse.TotalBullets > 0)
                {
                    anim.SetTrigger("reload");
                    if (ReloadButton != null)
                    {
                        ReloadButton.IsPressed = false;
                    }
                }
            }
        }
        /*
        //Weapon Switch
        if (IsDriving == false)
        { //if IsDriving == true cant change weapons
            if (Input.GetAxis("Mouse ScrollWheel") >= 0.05f)
            {
                if (WeaponID < Weapons.Length - 1)
                {
                    _NextWeapon();
                    InverseKinematics = true;
                }
            }
            if (Input.GetAxis("Mouse ScrollWheel") <= -0.05f)
            {
                if (WeaponID > -1)
                {
                    _PreviousWeapon();
                }
            }

            if (Input.GetKeyDown(KeyCode.E) || PressedNextWeapon)
            {
                _NextWeapon();
                if (NextWeaponButton != null)
                {
                    NextWeaponButton.IsPressed = false;
                }
            }
            if (Input.GetKeyDown(KeyCode.Q) || PressedPreviousWeapon)
            {
                if (PreviousWeaponButton != null)
                {
                    PreviousWeaponButton.IsPressed = false;
                }
                _PreviousWeapon();
            }
        }
        */
    }

    void MobileInputs()
    {
        //Inputs
        PressedShooting = ShotButton.IsPressed;
        PressedJump = JumpButton.IsPressed;
        PressedRun = RunButton.IsPressed;
        PressedPickupWeapon = PickWeaponButton.IsPressed;
        PressedRoll = RollButton.IsPressed;
        PressedEnterVehicle = EnterVehicleButton.IsPressed;
        PressedCrouch = CrouchButton.IsPressed;
        PressedReload = ReloadButton.IsPressed;
        PressedNextWeapon = NextWeaponButton.IsPressed;
        PressedPreviousWeapon = PreviousWeaponButton.IsPressed;
        //Joystick
        if (Input.GetAxis("Vertical") == 0 && Input.GetAxis("Horizontal") == 0)
        {
            HorizontalX = MovementJoystick.InputVector.x;
            VerticalY = MovementJoystick.InputVector.z;
        }
        if (IsRunning && HorizontalX == 0)
        {
            VerticalY = 1;
        }
        //Button
        if (VehicleInArea != null && IsDriving == false)
        {
            EnterVehicleButton.gameObject.SetActive(true);
        }
        else
        {
            EnterVehicleButton.gameObject.SetActive(false);
        }
        if (VehicleInArea != null && IsDriving == true)
        {
            EnterVehicleButton.gameObject.SetActive(true);
        }

        PickWeaponButton.gameObject.SetActive(ToPickupWeapon);
    }
    public bool fixedCameraSetupHack = true;
    void Movement()
    {
        if (fixedCameraSetupHack)
        {
            if (Mathf.Abs(LastX) < 0.01f && Mathf.Abs(HorizontalX) > 0.01f)
            {
                lastCameraTransform = FixedCameraZone.currentFixedCamera;
                VerticalY = 0;
            }
            else if (Mathf.Abs(LastY) < 0.01f && Mathf.Abs(VerticalY) > 0.01f)
            {
                lastCameraTransform = FixedCameraZone.currentFixedCamera;
                HorizontalX = 0;
            }
        }
        //Moving Check
        if (VerticalY > 0.01f || VerticalY < -0.01f || HorizontalX > 0.01f || HorizontalX < -0.01f)
        {
            IsMoving = true;
        }
        if (VerticalY == 0 && HorizontalX == 0)
        {
            IsMoving = false;
        }

        if (!IsArmed && IsDriving == false)
        {
            IsArmedWeight = Mathf.Lerp(IsArmedWeight, 0, 5 * Time.deltaTime);
            anim.SetFloat("velocity", VelocityMultiplier);
            if (IsGrounded && CanMove && IsAttacking == false)
            {
                rb.velocity = LookAtDirection.forward * VelocityMultiplier * Velocity + transform.up * rb.velocity.y;
            }
            if (IsMoving && IsAttacking == false)
            {
                if (IsRunning == true && WallInFront == false)
                {
                    VelocityMultiplier = Mathf.Lerp(VelocityMultiplier, runSpeedMult, 1 * Time.deltaTime);
                }
                else if (WallInFront == false)
                {
                    VelocityMultiplier = Mathf.Lerp(VelocityMultiplier, 0.5f, 5 * Time.deltaTime);
                }
            }
            else
            {
                VelocityMultiplier = Mathf.Lerp(VelocityMultiplier, 0.0f, 5 * Time.deltaTime);
                IsRunning = false;
            }
        }
        else if (IsArmed && WeaponInUse != null && IsDriving == false)
        {
            if (IsRolling == false)
            {
                IsArmedWeight = Mathf.Lerp(IsArmedWeight, 1, 5 * Time.deltaTime);
                var VY = Mathf.Lerp(anim.GetFloat("vertical"), 2 * VelocityMultiplier * 5 * VerticalY, 5 * Time.deltaTime);
                var HX = Mathf.Lerp(anim.GetFloat("horizontal"), 2 * VelocityMultiplier * 5 * HorizontalX, 5 * Time.deltaTime);
                anim.SetFloat("vertical", VY);
                anim.SetFloat("horizontal", HX);

                //Movement
                if (CanMove && IsGrounded)
                {
                    rb.velocity = LookAtDirection.forward * VelocityMultiplier * Velocity + transform.up * rb.velocity.y;
                }
                if (IsRunning == true && WallInFront == false && IsGrounded == true && IsMoving == true)
                {
                    VelocityMultiplier = Mathf.Lerp(VelocityMultiplier, 1f, 2 * Time.deltaTime);
                }
                if (IsRunning == false && WallInFront == false && IsGrounded == true && IsMoving == true)
                {
                    VelocityMultiplier = Mathf.Lerp(VelocityMultiplier, 0.5f, 5 * Time.deltaTime);
                }
                if (IsMoving == false)
                {
                    VelocityMultiplier = Mathf.Lerp(VelocityMultiplier, 0f, 5 * Time.deltaTime);
                }
            }
        }

        anim.SetLayerWeight(1, IsArmedWeight);
        anim.SetBool("armed", IsArmed);

        //Rolling
        if (IsRolling == true && WallInFront == false)
        {
            transform.Translate(0, 0, 1 * Time.fixedDeltaTime);
        }
        //Velocity On Jump
        if (IsGrounded)
        {
            LastX = HorizontalX;
            LastY = VerticalY;
            LastVelMult = VelocityMultiplier;
            CanMove = true;
        }
        else
        {
            transform.Translate(0, -1 * Time.deltaTime, 0);
            if (IsMoving)
            {
                rb.velocity = rb.velocity + LookAtDirection.forward * AirInfluenceControll / 10;
            }
        }
    }
    [HideInInspector] public RaycastHit Step_Hit;
    [HideInInspector] private bool AdjustHeight;
    void StepCorrect()
    {
        if (IsGrounded && IsMoving && EnableStepCorrection == true)
        {
            //Friction
            RaycastHit friction_hit;
            if (Physics.Raycast(transform.position + transform.up * 1.5f + LookAtDirection.forward * 0.3f, -transform.up, out friction_hit, 2f, WhatIsGround))
            {
                if (friction_hit.point.y + .05 < transform.position.y)
                {
                    //Apply Friction Force
                    rb.AddForce(0, -10, 0, ForceMode.Impulse);
                }
            }

            //Step height Correction
            if (Physics.Raycast(transform.position + transform.up * FootstepHeight + LookAtDirection.forward * StepOffset, -transform.up, out Step_Hit, FootstepHeight - StepHeight, WhatIsGround) && AdjustHeight == false)
            {
                if (Step_Hit.point.y > transform.position.y + .03f && AdjustHeight == false)
                {
                    AdjustHeight = true;
                }
            }
        }
        if (AdjustHeight && Step_Hit.point != Vector3.zero)
        {
            transform.position = Vector3.Lerp(transform.position, Step_Hit.point + transform.up * StepOffset, 8 * Time.deltaTime);
            if (transform.position == Step_Hit.point)
                AdjustHeight = false;
        }
    }
    void Rotate()
    {
        if (IsDriving == true || IsAttacking == true)
            return;
        //Rotation Direction
        Rotation = new Vector3(HorizontalX, 0, VerticalY);

        //LookAtDirection Transform
        if (IsMoving)
        {
            LookAtDirection.rotation = Quaternion.Lerp(LookAtDirection.rotation, Quaternion.LookRotation(Rotation) * eul, 50 * Time.deltaTime);
        }

        //Body Rotation
        if (EulerRotation.y + 0.5f < transform.eulerAngles.y)
        {
            EulerRotation.y = transform.eulerAngles.y;
            BodyRotation = Mathf.Lerp(BodyRotation, 0.1f, 1 * Time.deltaTime);
        }
        if (EulerRotation.y - 0.5f > transform.eulerAngles.y)
        {
            EulerRotation.y = transform.eulerAngles.y;
            BodyRotation = Mathf.Lerp(BodyRotation, -0.1f, 1 * Time.deltaTime);
        }
        if (EulerRotation.y > transform.eulerAngles.y - 0.1f && EulerRotation.y < transform.eulerAngles.y + 0.1f)
        {
            EulerRotation.y = transform.eulerAngles.y;
            BodyRotation = Mathf.Lerp(BodyRotation, 0f, 1 * Time.deltaTime);
        }
        anim.SetFloat("bodyrotation", BodyRotation);

        if (!IsArmed)
        {

            if (VerticalY > 0.01f || VerticalY < -0.01f || HorizontalX > 0.01f || HorizontalX < -0.01f)
            {
                if (CanMove && IsGrounded && WallInFront == false)
                {
                    transform.rotation = Quaternion.Slerp(transform.rotation, LookAtDirection.rotation, 20 * Time.deltaTime);
                }
            }
        }
        else if (IsArmed && WeaponInUse != null)
        {
            if (IsRolling == false)
            {
                transform.rotation = Quaternion.Slerp(transform.rotation, eul, 15 * Time.deltaTime);
            }
        }

        anim.SetLayerWeight(1, IsArmedWeight);
        anim.SetBool("armed", IsArmed);

        //Rolling
        if (IsRolling == true)
        {
            IsArmedWeight = Mathf.Lerp(IsArmedWeight, 0f, 5 * Time.deltaTime);
            InverseKinematics = false;
            CanMove = false;
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(Rotation) * eul, 8 * Time.deltaTime);
        }
    }
    void PunchAttack()
    {
        if (WeaponInUse == null && IsGrounded == true && IsArmed == false)
        {

            currenttimetodisable_isattacking = 0f;

            IsAttacking = true;
            anim.SetTrigger("punch");

            IsCrouched = false;
        }
    }
    void WeaponControll()
    {
        //FireRate
        if (CanShoot == false && WeaponInUse != null)
        {
            CurrentFireRateToShoot += Time.deltaTime;
            if (CurrentFireRateToShoot >= WeaponInUse.Fire_Rate)
            {
                CurrentFireRateToShoot = 0;
                CanShoot = true;
                Shot = false;
                shotRelaseTime = Time.time;
            }
        }
        //WeaponControll
        if (IsArmed && WeaponInUse != null)
        {
            ShotErrorProbability = Mathf.LerpUnclamped(ShotErrorProbability, 0, WeaponInUse.Precision * Time.deltaTime);
            if (WeaponInUse.BulletsAmounts == 0)
            {
                CanShoot = false;
                Shot = false;
                CurrentFireRateToShoot = 0;
            }

        }

        //If is Armed
        if (WeaponID != -1)
        {
            IsArmed = true;
        }
        else
        {
            IsArmed = false;
        }

        //WeaponID Clamp
        WeaponID = Mathf.Clamp(WeaponID, -1, Weapons.Length);
    }
    void PickUpWeapons()
    {
        RaycastHit hitweapon;
        if (Physics.Raycast(MyCamera.transform.position, MyCamera.transform.forward, out hitweapon, 5, WeaponLayer))
        {
            if (hitweapon.collider.gameObject.layer == 14)
            {
                ToPickupWeapon = true;

                if (Input.GetKeyDown(KeyCode.F) || PressedPickupWeapon)
                {
                    if (PickWeaponButton != null)
                    {
                        PickWeaponButton.IsPressed = false;
                    }
                    if (Weapons[hitweapon.transform.GetComponent<Weapon>().WeaponID].Unlocked == false)
                    {
                        Weapons[hitweapon.transform.GetComponent<Weapon>().WeaponID].Unlocked = true;
                        Weapons[hitweapon.transform.GetComponent<Weapon>().WeaponID].TotalBullets += hitweapon.transform.GetComponent<Weapon>().BulletsAmounts;
                        Destroy(hitweapon.transform.gameObject);
                    }
                    else
                    {
                        Weapons[hitweapon.transform.GetComponent<Weapon>().WeaponID].TotalBullets += hitweapon.transform.GetComponent<Weapon>().BulletsAmounts;
                        Destroy(hitweapon.transform.gameObject);
                    }
                }

                if (Input.GetKeyDown("joystick button 3") || PressedPickupWeapon)
                {
                    if (PickWeaponButton != null)
                    {
                        PickWeaponButton.IsPressed = false;
                    }
                    if (Weapons[hitweapon.transform.GetComponent<Weapon>().WeaponID].Unlocked == false)
                    {
                        Weapons[hitweapon.transform.GetComponent<Weapon>().WeaponID].Unlocked = true;
                        Weapons[hitweapon.transform.GetComponent<Weapon>().WeaponID].TotalBullets += hitweapon.transform.GetComponent<Weapon>().BulletsAmounts;
                        Destroy(hitweapon.transform.gameObject);
                    }
                    else
                    {
                        Weapons[hitweapon.transform.GetComponent<Weapon>().WeaponID].TotalBullets += hitweapon.transform.GetComponent<Weapon>().BulletsAmounts;
                        Destroy(hitweapon.transform.gameObject);
                    }
                }
            }
            else
            {
                ToPickupWeapon = false;
            }
        }
        else
        {
            ToPickupWeapon = false;
        }
    }
    void Shoot()
    {
        if (WeaponInUse != null)
        {
            Shot = true;
            if (WeaponInUse.Type != Weapon.WeaponType.Shotgun)
            {
                //Raycast Camera
                var BulletRotationPrecision = MyCamera.transform.forward;
                BulletRotationPrecision.x += Random.Range(-ShotErrorProbability / 2, ShotErrorProbability / 2);
                BulletRotationPrecision.y += Random.Range(-ShotErrorProbability / 2, ShotErrorProbability / 2);
                BulletRotationPrecision.z += Random.Range(-ShotErrorProbability / 2, ShotErrorProbability / 2);

                //Raycast Camera
                //Apply Acurracy error
                if (Physics.Raycast(MyCamera.transform.position + MyCamera.transform.forward * MyPivotCamera.Distance / 2, BulletRotationPrecision, out CrosshairHit, 500, CrosshairHitMask))
                {
                    WeaponInUse.Shoot_Position.LookAt(CrosshairHit.point);
                    Debug.DrawLine(MyCamera.transform.position + MyCamera.transform.forward * MyPivotCamera.Distance, CrosshairHit.point, Color.red);
                    ShotErrorProbability = ShotErrorProbability + WeaponInUse.LossOfAccuracyPerShot;
                }
                else
                {
                    WeaponInUse.Shoot_Position.rotation = MyCamera.transform.rotation;
                    ShotErrorProbability = ShotErrorProbability + WeaponInUse.LossOfAccuracyPerShot;
                }
                //Spawn bullet
                var bullet = (GameObject)Instantiate(WeaponInUse.BulletPrefab, WeaponInUse.Shoot_Position.position, WeaponInUse.Shoot_Position.rotation);
                bullet.GetComponent<Bullet>().FinalPoint = CrosshairHit.point;
                bullet.GetComponent<Bullet>().DestroyBulletRotation = CrosshairHit.normal;
                bullet.GetComponent<Bullet>().damage = WeaponInUse.damage;
                Destroy(bullet, 10f);

                //Spawn Bullet Casing
                if (WeaponInUse.BulletCasingPrefab != null)
                {
                    var bulletcasing = (GameObject)Instantiate(WeaponInUse.BulletCasingPrefab, WeaponInUse.GunSlider.position, WeaponInUse.transform.rotation);
                    bulletcasing.hideFlags = HideFlags.HideInHierarchy;
                    Destroy(bulletcasing, 5f);
                }
            }
            else
            {
                for (int i = 0; i < 6; i++)
                {
                    var BulletRotationPrecision = MyCamera.transform.forward;
                    BulletRotationPrecision.x += Random.Range(-WeaponInUse.LossOfAccuracyPerShot, WeaponInUse.LossOfAccuracyPerShot);
                    BulletRotationPrecision.y += Random.Range(-WeaponInUse.LossOfAccuracyPerShot, WeaponInUse.LossOfAccuracyPerShot);
                    BulletRotationPrecision.z += Random.Range(-WeaponInUse.LossOfAccuracyPerShot, WeaponInUse.LossOfAccuracyPerShot);
                    ShotErrorProbability = ShotErrorProbability + 5 * WeaponInUse.LossOfAccuracyPerShot;
                    if (Physics.Raycast(MyCamera.transform.position + MyCamera.transform.forward * MyPivotCamera.Distance / 2, BulletRotationPrecision, out CrosshairHit, 500, CrosshairHitMask))
                    {
                        WeaponInUse.Shoot_Position.LookAt(CrosshairHit.point);
                        Debug.DrawLine(MyCamera.transform.position + MyCamera.transform.forward * MyPivotCamera.Distance, CrosshairHit.point, Color.red);

                        var bullet = (GameObject)Instantiate(WeaponInUse.BulletPrefab, WeaponInUse.Shoot_Position.position, WeaponInUse.Shoot_Position.rotation);
                        bullet.GetComponent<Bullet>().FinalPoint = CrosshairHit.point;
                        bullet.GetComponent<Bullet>().DestroyBulletRotation = CrosshairHit.normal;
                        bullet.GetComponent<Bullet>().damage = WeaponInUse.damage;
                        Destroy(bullet, 10f);
                    }
                    else
                    {
                        var BulletRotationPrecisionShootgun = MyCamera.transform.rotation;
                        BulletRotationPrecisionShootgun.x += Random.Range(-WeaponInUse.LossOfAccuracyPerShot, WeaponInUse.LossOfAccuracyPerShot);
                        BulletRotationPrecisionShootgun.y += Random.Range(-WeaponInUse.LossOfAccuracyPerShot, WeaponInUse.LossOfAccuracyPerShot);
                        BulletRotationPrecisionShootgun.z += Random.Range(-WeaponInUse.LossOfAccuracyPerShot, WeaponInUse.LossOfAccuracyPerShot);
                        BulletRotationPrecisionShootgun.w += Random.Range(-WeaponInUse.LossOfAccuracyPerShot, WeaponInUse.LossOfAccuracyPerShot);

                        var bullet = (GameObject)Instantiate(WeaponInUse.BulletPrefab, WeaponInUse.Shoot_Position.position, BulletRotationPrecisionShootgun);
                        Destroy(bullet, 10f);
                    }
                }
            }
            WeaponInUse.Shoot_Position.rotation = MyCamera.transform.rotation;

            var muzzleflesh = (GameObject)Instantiate(WeaponInUse.MuzzleFlashParticlePrefab, WeaponInUse.Shoot_Position.position, WeaponInUse.Shoot_Position.rotation);
            Destroy(muzzleflesh, 2);

            CurrentFireRateToShoot = 0;
            CanShoot = false;

            WeaponInUse.BulletsAmounts -= 1;

            if (WeaponInUse.GenerateProceduralAnimation == true)
            {
                Invoke("WeaponRecoil", 0.06f);
            }

            if (WeaponInUse.ShootAudio != null)
            {
                WeaponInUse.GetComponent<AudioSource>().pitch = Random.Range(1.0f, 1.05f);
                WeaponInUse.GetComponent<AudioSource>().PlayOneShot(WeaponInUse.ShootAudio);
            }
        }
    }
    void DriveControll()
    {
        //Driving Control
        anim.SetBool("driving", IsDriving);
        if (IsDriving == true && VehicleInArea != null)
        {
            CanMove = false;
            IsJumping = false;
            IsCrouched = false;
            VelocityMultiplier = 0;

            //SimulateInert
            rb.velocity = VehicleInArea.rb.velocity;

            IsArmedWeight = 0;
            WeaponID = -1;
            IsArmed = false;
            if (WeaponInUse != null)
            {
                WeaponInUse.gameObject.SetActive(false);
                WeaponInUse = null;
            }

            anim.SetBool("crouched", false);
            anim.SetLayerWeight(1, IsArmedWeight);
            anim.SetBool("armed", IsArmed);
            anim.SetBool("isgrounded", true);
            anim.SetBool("running", false);
            anim.SetFloat("bodyrotation", 0);


            transform.position = VehicleInArea.PlayerLocation.position;
            transform.rotation = VehicleInArea.PlayerLocation.rotation;

            GetComponent<Collider>().isTrigger = true;
            rb.useGravity = false;


            VehicleInArea.IsOn = true;
        }
        else
        {
            GetComponent<Collider>().isTrigger = false;
            rb.useGravity = true;
            if (VehicleInArea != null)
            {
                VehicleInArea.IsOn = false;
            }
        }
        if (VehicleInArea != null)
        {
            if (Input.GetKeyDown(KeyCode.F) || PressedEnterVehicle)
            {
                if (EnterVehicleButton != null)
                {
                    EnterVehicleButton.IsPressed = false;
                }

                if (IsDriving == false)
                {
                    ToEnterVehicle = false;
                    IsDriving = true;
                }
                else
                {
                    IsDriving = false;
                    transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
                    if (VehicleInArea.TypeOfVehicle == Vehicle.VehicleType.Car)
                    {
                        transform.position = VehicleInArea.transform.position - transform.right * 1.2f;
                    }
                    else
                    {
                        transform.position = VehicleInArea.transform.position - transform.right * 0.5f;
                    }
                }
            }
        }
        else
        {
            IsDriving = false;
        }
    }
    void WeaponIKControll()
    {
        if (IsRolling == false && IsArmed)
        {
            //Pivot Weapons Rotation
            PivotWeapons.transform.position = HumanoidSpine.position;
            PivotWeapons.transform.rotation = Quaternion.Euler(PivotWeapons.transform.eulerAngles.x, transform.eulerAngles.y, PivotWeapons.transform.eulerAngles.z);
            PivotWeapons.transform.rotation = Quaternion.Lerp(PivotWeapons.transform.rotation, MyCamera.transform.rotation, 15 * Time.deltaTime);
        }
        //Weapon and Hands Rotation
        if (IsArmed && WeaponInUse != null)
        {
            //IK Hands RIGHT
            if (WeaponInUse.Type != Weapon.WeaponType.Shotgun)
            {
                //Rifle IK TARGET
                Type_RifleIK_RHand_Pos.localPosition = Vector3.Lerp(Type_RifleIK_RHand_Pos.localPosition, StartRifleIKPosition, 20 * Time.deltaTime);
                Type_RifleIK_RHand_Pos.localRotation = Quaternion.Lerp(Type_RifleIK_RHand_Pos.localRotation, StartRifleIKRotation, 20 * Time.deltaTime);
                //Pistol IK TARGET
                Type_PistolIK_RHand_Pos.localPosition = Vector3.Lerp(Type_PistolIK_RHand_Pos.localPosition, StartPistolIKPosition, 20 * Time.deltaTime);
                Type_PistolIK_RHand_Pos.localRotation = Quaternion.Lerp(Type_PistolIK_RHand_Pos.localRotation, StartPistolIKRotation, 20 * Time.deltaTime);
            }
            else
            {
                //Rifle IK TARGET
                Type_RifleIK_RHand_Pos.localPosition = Vector3.Lerp(Type_RifleIK_RHand_Pos.localPosition, StartRifleIKPosition, 8 * Time.deltaTime);
                Type_RifleIK_RHand_Pos.localRotation = Quaternion.Lerp(Type_RifleIK_RHand_Pos.localRotation, StartRifleIKRotation, 8 * Time.deltaTime);
                //Pistol IK TARGET
                Type_PistolIK_RHand_Pos.localPosition = Vector3.Lerp(Type_PistolIK_RHand_Pos.localPosition, StartPistolIKPosition, 8 * Time.deltaTime);
                Type_PistolIK_RHand_Pos.localRotation = Quaternion.Lerp(Type_PistolIK_RHand_Pos.localRotation, StartPistolIKRotation, 8 * Time.deltaTime);
            }
        }
        if (WeaponInUse != null)
        {
            //If Weapon Type is Pistol Type
            if (WeaponInUse.Type == Weapon.WeaponType.Pistol || WeaponInUse.Type == Weapon.WeaponType.PistolSpecial || WeaponInUse.Type == Weapon.WeaponType.Rocket)
            {
                IKPositionRHAND.position = Type_PistolIK_RHand_Pos.position;
                IKPositionRHAND.rotation = Type_PistolIK_RHand_Pos.rotation;
            }
            //If Weapon Type is Rifle and Shotgun Type
            if (WeaponInUse.Type == Weapon.WeaponType.Rifle || WeaponInUse.Type == Weapon.WeaponType.Shotgun || WeaponInUse.Type == Weapon.WeaponType.Flamethrower)
            {
                IKPositionRHAND.position = Type_RifleIK_RHand_Pos.position;
                IKPositionRHAND.rotation = Type_RifleIK_RHand_Pos.rotation;
            }
            IKPositionLHAND.position = WeaponInUse.IK_Position_LeftHand.transform.position;
            IKPositionLHAND.rotation = WeaponInUse.IK_Position_LeftHand.rotation;
        }
        //IKActive
        if (InverseKinematics == true)
        {
            WeightIK = Mathf.Lerp(WeightIK, 1, 5 * Time.deltaTime);
        }
        else
        {
            WeightIK = Mathf.Lerp(WeightIK, 0, 3 * Time.deltaTime);
        }
    }

    private void Footsteps()
    {
        if (IsGrounded == true && IsRolling == false && IsDriving == false && CanMove == true && FootstepAudioClips.Length > 0)
        {
            if (HorizontalX > 0.02f || HorizontalX < -0.02f || VerticalY > 0.02f || VerticalY < -0.02f)
            {

                currentFootstepTime += VelocityMultiplier * Time.deltaTime;
                if (currentFootstepTime >= MaxFootstepTime)
                {
                    AudioS.PlayOneShot(FootstepAudioClips[Random.Range(0, FootstepAudioClips.Length)]);
                    currentFootstepTime = 0;
                }
            }
            else
            {
                currentFootstepTime = 0;
            }
        }
    }
    #endregion

    #region Invoke Events
    void WeaponRecoil()
    {
        if (WeaponInUse != null)
        {
            Type_RifleIK_RHand_Pos.Translate(0, 0, -WeaponInUse.RecoilForce);
            Type_RifleIK_RHand_Pos.Rotate(0, -WeaponInUse.RecoilForceRotation, 0);
            Type_PistolIK_RHand_Pos.Translate(0, 0, -WeaponInUse.RecoilForce);
            Type_PistolIK_RHand_Pos.Rotate(0, -WeaponInUse.RecoilForceRotation, 0);

            if (MyPivotCamera.CameraShakeWhenShooting == true)
            {
                if (WeaponInUse.Type == Weapon.WeaponType.Shotgun)
                {
                    MyPivotCamera.rotX -= MyPivotCamera.CameraShakeSensibility * 20 * WeaponInUse.RecoilForce;
                }
                else
                {
                    MyPivotCamera.rotX -= MyPivotCamera.CameraShakeSensibility * 20 * WeaponInUse.RecoilForce;
                }
            }
        }
    }
    private void _disablejump()
    {
        IsJumping = false;
    }
    private void _disableroll()
    {
        IsRolling = false;
    }
    #endregion

    #region Animation Events
    public void reload()
    {
        Reload(WeaponInUse);
    }
    public void Reload(Weapon w)
    {
        int ammountToReload = w.BulletsPerMagazine - w.BulletsAmounts;
        if (ammountToReload > 0)
        {
            if (w.TotalBullets >= ammountToReload)
            {
                w.BulletsAmounts = w.BulletsPerMagazine;
                w.TotalBullets -= ammountToReload;
            }
            else
            {
                w.BulletsAmounts += w.TotalBullets;
                w.TotalBullets = 0;
            }
        }
        w.GetComponent<AudioSource>().PlayOneShot(w.ReloadAudio);
    }
    public void nomove()
    {
        CanMove = false;
    }
    public void move()
    {
        CanMove = true;
    }
    public void uppunch()
    {
        IsAttacking = true;
        currenttimetodisable_isattacking = 0;
        print("Attacked with a punch up");
        var lefthand = anim.GetBoneTransform(HumanBodyBones.LeftLowerArm);
        RaycastHit hit;
        if (Physics.Raycast(lefthand.transform.position - transform.forward * 0.5f, transform.forward, out hit, .95f, WhatIsGround))
        {
            var particle = (GameObject)Instantiate(PunchCollisionParticle, hit.point, lefthand.rotation);
            Destroy(particle, 1f);
            Debug.DrawLine(hit.point, lefthand.transform.position - transform.forward * 0.5f);
        }
    }
    public void Rpunch()
    {
        IsAttacking = true;
        currenttimetodisable_isattacking = 0;
        print("Attacked with a right punch");

        var righthand = anim.GetBoneTransform(HumanBodyBones.RightLowerArm);
        RaycastHit hit;
        if (Physics.Raycast(righthand.transform.position - transform.forward * 0.5f, transform.forward, out hit, .95f, WhatIsGround))
        {
            var particle = (GameObject)Instantiate(PunchCollisionParticle, hit.point, righthand.rotation);
            Destroy(particle, 1f);
            Debug.DrawLine(hit.point, righthand.transform.position - transform.forward * 0.5f);
        }
    }
    public void Lpunch()
    {
        IsAttacking = true;
        currenttimetodisable_isattacking = 0;
        print("Attacked with a left punch");

        var lefthand = anim.GetBoneTransform(HumanBodyBones.LeftLowerArm);
        RaycastHit hit;
        if (Physics.Raycast(lefthand.transform.position - transform.forward * 0.5f, transform.forward, out hit, .95f, WhatIsGround))
        {
            var particle = (GameObject)Instantiate(PunchCollisionParticle, hit.point, lefthand.rotation);
            Destroy(particle, 1f);
            Debug.DrawLine(hit.point, lefthand.transform.position - transform.forward * 0.5f);

        }
    }
    public void noik()
    {
        InverseKinematics = false;
    }
    public void ik()
    {
        InverseKinematics = true;
    }
    public void rollout()
    {
        CanMove = true;
        IsRolling = false;
        ik();
    }
    public void roll()
    {
        IsRolling = true;
        noik();
    }
    #endregion

    #region Physics Check
    void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "VehicleArea")
        {
            VehicleInArea = other.GetComponentInParent<Vehicle>();
            ToEnterVehicle = true;
        }
        if (other.gameObject.tag == "DeadZone")
        {
            Life = 0;
            print("Killed by Dead Zone");
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.transform.tag == "VehicleArea" && IsDriving == false)
        {
            VehicleInArea = null;
            ToEnterVehicle = false;
        }
    }
    #endregion

    void OnAnimatorIK(int layerIndex)
    {
        if (layerIndex == 0)
        {
            if (IsDead == false)
            {
                if (WeaponInUse != null && IsRolling == false && IsDriving == false)
                {
                    anim.SetIKRotationWeight(AvatarIKGoal.RightHand, WeightIK);
                    anim.SetIKPositionWeight(AvatarIKGoal.RightHand, WeightIK);

                    anim.SetIKRotationWeight(AvatarIKGoal.LeftHand, WeightIK);
                    anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, WeightIK);

                    anim.SetIKPosition(AvatarIKGoal.RightHand, IKPositionRHAND.position);
                    anim.SetIKRotation(AvatarIKGoal.RightHand, IKPositionRHAND.rotation);

                    anim.SetIKPosition(AvatarIKGoal.LeftHand, IKPositionLHAND.position);
                    anim.SetIKRotation(AvatarIKGoal.LeftHand, IKPositionLHAND.rotation);

                    anim.SetLookAtWeight(WeightIK, WeightIK / 2, WeightIK);
                    anim.SetLookAtPosition(MyCamera.transform.position + MyCamera.transform.forward * 20f);
                }
                if (IsDriving)
                {
                    anim.SetIKRotationWeight(AvatarIKGoal.RightHand, 1);
                    anim.SetIKPositionWeight(AvatarIKGoal.RightHand, 1);
                    anim.SetIKRotationWeight(AvatarIKGoal.LeftHand, 1);
                    anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, 1);

                    anim.SetIKRotationWeight(AvatarIKGoal.RightFoot, 1);
                    anim.SetIKPositionWeight(AvatarIKGoal.RightFoot, 1);
                    anim.SetIKRotationWeight(AvatarIKGoal.LeftFoot, 1);
                    anim.SetIKPositionWeight(AvatarIKGoal.LeftFoot, 1);

                    anim.SetLookAtWeight(0.9f, 0.0f, 0.8f);
                    anim.SetLookAtPosition(MyCamera.transform.position + MyCamera.transform.forward * 50);

                    anim.SetIKPosition(AvatarIKGoal.RightHand, VehicleInArea.RightHandPositionIK.position);
                    anim.SetIKRotation(AvatarIKGoal.RightHand, VehicleInArea.RightHandPositionIK.rotation);
                    anim.SetIKPosition(AvatarIKGoal.LeftHand, VehicleInArea.LeftHandPositionIK.position);
                    anim.SetIKRotation(AvatarIKGoal.LeftHand, VehicleInArea.LeftHandPositionIK.rotation);

                    anim.SetIKPosition(AvatarIKGoal.RightFoot, VehicleInArea.RightFootPositionIK.position);
                    anim.SetIKRotation(AvatarIKGoal.RightFoot, VehicleInArea.RightFootPositionIK.rotation);
                    anim.SetIKPosition(AvatarIKGoal.LeftFoot, VehicleInArea.LeftFootPositionIK.position);
                    anim.SetIKRotation(AvatarIKGoal.LeftFoot, VehicleInArea.LeftFootPositionIK.rotation);
                }
            }
        }
    }
}
